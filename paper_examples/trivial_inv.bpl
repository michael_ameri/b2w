procedure trivial_inv()
{
	var i: int;
	i := 0;
	while (i < 10)
  	invariant (0 <= i && i <= 10);
	invariant (exists j: int :: i == 2*j);
	{ 				
	  i := i + 2;
  }	
}			
