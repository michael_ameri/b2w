package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.types.Type;
import whyML.ast.types.TypeVariable;
import whyML.astvisitor.ASTVisitor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TypeDeclaration extends Declaration{


    private String lident;
    private TypeVariable[] typeParams;
    //aliasType can be null, if it is an abstract type.
    private Type aliasType;


    public TypeDeclaration(String lident, TypeVariable[] typeParams, Type aliasType){
        this.lident = lident;
        this.typeParams = typeParams;
        this.aliasType = aliasType;
    }

    /**
     * Use this constructor if there are no type variables, and no aliases.
     * @param lident
     */
    public TypeDeclaration(String lident){
        this.lident = lident;
        this.typeParams = new TypeVariable[0];
        this.aliasType = null;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    public Type getAliasType() {
        return aliasType;
    }

    public TypeVariable[] getTypeParams() {
        return typeParams;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(Arrays.asList(typeParams));
        if(aliasType != null){
            children.add(aliasType);
        }
        return children;
    }
}
