package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.ImportExport;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

//use imp-exp tqualid (as uident)?
public class Use extends Declaration {

    //is the use declared as import, export, or none.
    private ImportExport impEmp;
    private String useName;
    private Boolean hasAsName;
    private String asName;

    public Use(ImportExport impEmp, String useName, String asName){
        this.impEmp = impEmp;
        this.useName = useName;
        this.asName = asName;
        this.hasAsName = true;
    }

    public Use(ImportExport impEmp, String useName){
        this.impEmp = impEmp;
        this.useName = useName;
        this.asName = null;
        this.hasAsName = false;
    }

    public ImportExport getImpEmp() {
        return impEmp;
    }

    public String getAsName() {
        return asName;
    }

    public String getUseName() {
        return useName;
    }

    public Boolean getHasAsName(){
        return hasAsName;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        String result = "use ";
        if (impEmp == ImportExport.IMPORT){
            result = result + "import ";
        }else if (impEmp == ImportExport.EXPORT){
            result = result + "export ";
        }
        result = result + useName + " ";
        if(hasAsName){
            result = result + asName + " ";
        }
        return result;
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
