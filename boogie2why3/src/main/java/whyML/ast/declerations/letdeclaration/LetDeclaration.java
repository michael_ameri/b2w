package whyML.ast.declerations.letdeclaration;

import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.declerations.Declaration;
import whyML.ast.expression.Expression;
import whyML.ast.spec.Spec;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * let ghost? lident label* pgm-defn
 */
public class LetDeclaration extends Declaration{
    private boolean isGhost;
    private String lident;
    private Label label;
    private PgmDefn pgmDefn;

    public LetDeclaration(boolean isGhost, String lident, Label label, PgmDefn pgmDefn) {
        this.isGhost = isGhost;
        this.lident = lident;
        this.label = label;
        this.pgmDefn = pgmDefn;
    }

    /**
     * sets ghost to false, and label to no label
     * @param lident
     * @param pgmDefn
     */
    public LetDeclaration(String lident, PgmDefn pgmDefn) {
        this.lident = lident;
        this.pgmDefn = pgmDefn;
        this.isGhost = false;
        label = new Label(false);
    }

    /**
     * constructor for easier syntax.
     * @param lident
     * @param inputParams
     * @param returnType
     * @param specs
     * @param expr
     */
    public LetDeclaration(String lident, List<Param> inputParams, Type returnType, List<Spec> specs, Expression expr){
        isGhost = false;
        label = new Label(false);
        pgmDefn = new FunBody(inputParams,returnType,specs,expr);
        this.lident = lident;
    }

    public boolean isGhost() {
        return isGhost;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    public Label getLabel() {
        return label;
    }

    public PgmDefn getPgmDefn() {
        return pgmDefn;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> result = new LinkedList<>();
        result.add(label);
        result.add(pgmDefn);
        return result;
    }
}
