package whyML.ast.declerations.letdeclaration;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class RecDefn implements ASTNode {

    //must contain AT LEAST 1
    private List<FunDefn> funDefns;

    public RecDefn(List<FunDefn> funDefns){
        this.funDefns = funDefns;
    }

    public RecDefn(FunDefn funDefn){
        this.funDefns = new LinkedList<FunDefn>();
        funDefns.add(funDefn);
    }

    public List<FunDefn> getFunDefns() {
        return funDefns;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(funDefns);
        return children;
    }
}
