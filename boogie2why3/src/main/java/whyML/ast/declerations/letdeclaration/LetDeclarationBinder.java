package whyML.ast.declerations.letdeclaration;

import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class LetDeclarationBinder implements ASTNode {

    private Boolean isParam;

    //this is ignored if isParam is false.
    private Param param;

    //these are ignored if isParam is true.
    private Boolean isGhost;
    private String lident;
    private Label label;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        if(isParam){
            children.add(param);
        }else{
            children.add(label);
        }
        return children;
    }

    public LetDeclarationBinder(Param param){
        isParam = true;
        this.param = param;
    }

    public LetDeclarationBinder(boolean isGhost, String lident, Label label){
        this.isParam = false;
        this.isGhost = isGhost;
        this.lident = lident;
        this.label = label;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    public Label getLabel() {
        return label;
    }

    public Param getParam() {
        return param;
    }

    public Boolean getIsGhost() {
        return isGhost;
    }

    public Boolean getIsParam() {
        return isParam;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
