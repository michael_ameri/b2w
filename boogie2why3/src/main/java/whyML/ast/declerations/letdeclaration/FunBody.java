package whyML.ast.declerations.letdeclaration;

import whyML.ast.ASTNode;
import whyML.ast.Param;
import whyML.ast.expression.Expression;
import whyML.ast.spec.Spec;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class FunBody extends PgmDefn {

    //must contain at least 1. (these are the input arguments)
    List<LetDeclarationBinder> binders;
    //return type is optional, can be null.
    Type returnType;
    //can be empty list
    List<Spec> specs;
    //the expressions of the body.
    Expression expr;



    /**
     * default constructor with all options
     * @param returnType
     * @param binders
     * @param specs
     * @param expr
     */
    public FunBody( Type returnType,List<LetDeclarationBinder> binders, List<Spec> specs, Expression expr){
        this.binders = binders;
        this.returnType = returnType;
        this.specs = specs;
        this.expr = expr;
    }

    /**
     * if we know all input arguments are params (i.e. have return types), use this constructor.
     * @param params
     * @param returnType
     * @param specs
     * @param expr
     */
    public FunBody(List<Param> params, Type returnType, List<Spec> specs, Expression expr){

        List<LetDeclarationBinder> binders = new LinkedList<LetDeclarationBinder>();
        for(Param p: params){
            binders.add(new LetDeclarationBinder(p));
        }
        this.binders = binders;
        this.returnType = returnType;
        this.specs = specs;
        this.expr = expr;
    }

    public Expression getExpr() {
        return expr;
    }

    public List<LetDeclarationBinder> getBinders() {
        return binders;
    }

    public List<Spec> getSpecs() {
        return specs;
    }

    public Type getReturnType() {
        return returnType;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> result = new LinkedList<>();
        result.addAll(binders);
        if(returnType != null){
            result.add(returnType);
        }
        result.addAll(specs);
        result.add(expr);
        return result;
    }
}
