package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ValGlobalVariable extends Val {

    private Boolean isGhost;
    private String lident;
    private Label label;
    private Type type;


    public ValGlobalVariable(Boolean isGhost, String lident, Label label, Type type ){
        this.isGhost = isGhost;
        this.lident = lident;
        this.label = label;
        this.type = type;
    }

    public Boolean getIsGhost() {
        return isGhost;
    }

    public Label getLabel() {
        return label;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    public Type getType() {
        return type;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(label);
        children.add(type);
        return children;
    }
}
