package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.terms.Term;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ConstantDeclaration extends Declaration {

    private String lident;
    private Label label;
    private Type type;
    //the term can be null, if no term exists.
    //e.g. constant a:int =0
    private Term equalityTerm;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(label);
        children.add(type);
        if(equalityTerm != null){
            children.add(equalityTerm);
        }
        return children;
    }

    public ConstantDeclaration(String lident, Label label, Type type, Term equalityTerm){
        this.lident = lident;
        this.label = label;
        this.type = type;
        this.equalityTerm = equalityTerm;
    }

    /**
     * create a constant declaration witouht label and equality term.
     * @param lident
     * @param type
     */
    public ConstantDeclaration(String lident, Type type){
        this.lident = lident;
        this.label = new Label(false);
        this.type = type;
        this.equalityTerm = null;
    }

    public Label getLabel() {
        return label;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    public Term getEqualityTerm() {
        return equalityTerm;
    }

    public Type getType() {
        return type;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
