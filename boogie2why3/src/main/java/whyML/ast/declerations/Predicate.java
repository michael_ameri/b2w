package whyML.ast.declerations;

import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.formula.Formula;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class Predicate extends Declaration {

    //can be null if there is no formula
    private Formula optionalFormula;
    private String name;
    private Label label;
    private List<Param> params;
    //true, iff the ident ofthis predicate is an infix or prefix operator.
    //e.g. predicate (<:) int int
    private Boolean isInfixOrPrefix;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        if(optionalFormula != null){
            children.add(optionalFormula);
        }
        children.add(label);
        children.addAll(params);
        return children;
    }

    public Predicate(String name, Label label, List<Param> params, Formula formula, Boolean isInfixOrPrefix){
        this.name = name;
        this.label = label;
        this.params = params;
        this.optionalFormula = formula;
        this.isInfixOrPrefix = isInfixOrPrefix;
    }

    /**
     * Create a predice without label and without a formula. (i.e. there is no equal sign after the predicate definition)
     * @param name
     * @param params
     */
    public Predicate(String name, List<Param> params, Boolean isInfixOrPrefix){
        //TODO add "with" clause
        this.name = name;
        this.params = params;
        this.optionalFormula = null;
        this.label = new Label(false);
        this.isInfixOrPrefix = isInfixOrPrefix;
    }

    /**
     * Create a predicate without a label.
     * @param name
     * @param params
     * @param formula
     */
    public Predicate(String name, List<Param> params, Formula formula, Boolean isInfixOrPrefix){
        this.name = name;
        this.label = new Label(false);
        this.params = params;
        this.optionalFormula = formula;
        this.isInfixOrPrefix = isInfixOrPrefix;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public Label getLabel() {
        return label;
    }

    public String getName() {
        return name;
    }

    public Formula getOptionalFormula() {
        return optionalFormula;
    }

    public List<Param> getParams() {
        return params;
    }

    public Boolean getIsInfixOrPrefix() {
        return isInfixOrPrefix;
    }

    public void setName(String name) {
        this.name = name;
    }
}
