package whyML.ast.pattern;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class TuplePattern extends Pattern {

    //must contain AT LEAST 2 patterns to be valid syntax.
    List<Pattern> patterns;

    public TuplePattern(List<Pattern> patterns){
        this.patterns = patterns;
    }


    public List<Pattern> getPatterns() {
        return patterns;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(patterns);
        return children;
    }
}
