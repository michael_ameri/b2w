package whyML.ast.formula.quantifier;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Trigger implements ASTNode{

    //must contain at least 1.
    //a trigger term is either a term or a formula.
    private List<TriggerTerm> triggerTerms;

    public Trigger(List<TriggerTerm> triggerTerms) {
        this.triggerTerms = triggerTerms;
    }

    public Trigger(TriggerTerm triggerTerm){
        this.triggerTerms = new LinkedList<>();
        triggerTerms.add(triggerTerm);
    }

    public List<TriggerTerm> getTriggerTerms() {
        return triggerTerms;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(triggerTerms);
        return children;
    }
}
