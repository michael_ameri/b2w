package whyML.ast.formula.quantifier;


import whyML.ast.ASTNode;
import whyML.ast.formula.Formula;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class QuantifiedFormula extends Formula {

    //true -> forall, false -> exists
    private Boolean isForall;

    //must be at least 1 binder to be legal syntax
    private List<FormulaBinder> binders;

    //can be empty
    private List<Trigger> triggers;

    private Formula formula;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(binders);
        children.addAll(triggers);
        children.add(formula);
        return children;
    }

    public QuantifiedFormula(Boolean isForall, List<FormulaBinder> binders, List<Trigger> triggers, Formula formula){
        this.isForall = isForall;
        this.binders = binders;
        this. triggers = triggers;
        this.formula = formula;
    }

    /**
     * create a quantified formula witout triggers.
     * @param isForall
     * @param binders
     * @param formula
     */
    public QuantifiedFormula(Boolean isForall, List<FormulaBinder> binders,  Formula formula){
        this.isForall = isForall;
        this.binders = binders;
        this. triggers = new LinkedList<Trigger>();
        this.formula = formula;
    }

    /**
     * create a quantified formula witout triggers.
     * @param isForall
     * @param binder
     * @param formula
     */
    public QuantifiedFormula(Boolean isForall, FormulaBinder binder,  Formula formula){
        this.isForall = isForall;
        this.binders = new LinkedList<>();
        this.binders.add(binder);
        this. triggers = new LinkedList<Trigger>();
        this.formula = formula;
    }

    public Boolean getIsForall() {
        return isForall;
    }

    public Formula getFormula() {
        return formula;
    }

    public List<FormulaBinder> getBinders() {
        return binders;
    }

    public List<Trigger> getTriggers() {
        return triggers;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
