package whyML.ast.formula.quantifier;


import whyML.ast.ASTNode;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FormulaBinder implements ASTNode{

    //must contain at least 1 element to be valid syntax.
    private List<String> idents;
    private Type type;


    public FormulaBinder(List<String> idents, Type type){
        this.idents = idents;
        this.type = type;
    }

    public FormulaBinder(String ident, Type type){
        this.idents = new LinkedList<String>();
        this.idents.add(ident);
        this.type = type;
    }

    public List<String> getIdents() {
        return idents;
    }

    public void setIdents(List<String> idents) {
        this.idents = idents;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        String res = idents.get(0);
        for(int i = 1; i < idents.size(); ++i){
            res = res.concat(" "+idents.get(i));
        }
        res = res.concat(": "+type.getTypeName()+" ");
        return res;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(type);
        return children;
    }
}
