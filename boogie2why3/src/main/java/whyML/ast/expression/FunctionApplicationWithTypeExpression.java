package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FunctionApplicationWithTypeExpression extends Expression{


    //the function identifier usually
    private Expression firstExpression;
    //must contain at least 1 argument (can be ())
    private List<Expression> arguments;

    private Type declaredType;

    public FunctionApplicationWithTypeExpression(Expression firstExpression, List<Expression> arguments, Type declaredType){
        this.firstExpression = firstExpression;
        this.arguments = arguments;
        this.declaredType = declaredType;
    }

    public FunctionApplicationWithTypeExpression(Expression firstExpression, Expression argument, Type declaredType){
        this.firstExpression = firstExpression;
        this.arguments = new LinkedList<>();
        arguments.add(argument);
        this.declaredType = declaredType;
    }

        public Expression getFirstExpression() {
        return firstExpression;
    }

    public List<Expression> getArguments() {
        return arguments;
    }

    public Type getDeclaredType() {
        return declaredType;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(firstExpression);
        children.addAll(arguments);
        children.add(declaredType);
        return children;
    }

}
