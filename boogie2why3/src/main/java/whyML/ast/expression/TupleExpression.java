package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class TupleExpression extends Expression{

    //must contain at least 2 to be valid syntax
    private List<Expression> expressions;

    public TupleExpression(List<Expression> expressions){
        this.expressions = expressions;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(expressions);
        return children;
    }
}
