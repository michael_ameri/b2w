package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class PrefixExpression extends Expression {

    private String prefixOperator;
    private Expression subExpression;

    public PrefixExpression(String prefixOperator, Expression subExpression) {
        this.prefixOperator = prefixOperator;
        this.subExpression = subExpression;
    }

    public String getPrefixOperator() {
        return prefixOperator;
    }

    public Expression getSubExpression() {
        return subExpression;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(subExpression);
        return children;
    }
}
