package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ConditionalExpression extends Expression{
    private Expression condition;
    private Expression thenExpression;
    //can be null.
    private Expression elseExpression;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(condition);
        children.add(thenExpression);
        if(elseExpression != null){
            children.add(elseExpression);
        }
        return children;
    }

    public ConditionalExpression(Expression condition, Expression thenExpression, Expression elseExpression){
        this.condition = condition;
        this.thenExpression = thenExpression;
        this.elseExpression = elseExpression;
    }

    public Expression getCondition() {
        return condition;
    }

    public Expression getElseExpression() {
        return elseExpression;
    }

    public Expression getThenExpression() {
        return thenExpression;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
