package whyML.ast.types;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * should contian at least 2 types, to be a valid tuple type. Is of the form (t1, t2, t3)
 */
public class TypeTuple extends Type{

    private Type type1;
    private Type type2;
    //can be empty, shouldn't be null.
    private List<Type> otherTypes;
    private boolean isRef;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(type1);
        children.add(type2);
        children.addAll(otherTypes);
        return children;
    }

    public TypeTuple(Type type1, Type type2, List<Type> otherTypes){
        this.type1 = type1;
        this.type2 = type2;
        this.otherTypes = otherTypes;
        this.isRef = false;
    }

    /**
     * list must contain AT LEAST 2 elements to be a valid tuple type.
     * @param otherTypes
     */
    public TypeTuple(List<Type> otherTypes){
        this.type1 = otherTypes.get(0);
        this.type2 = otherTypes.get(1);
        if(otherTypes.size() > 2){
            this.otherTypes = otherTypes.subList(2,otherTypes.size());
        }else{
            this.otherTypes = new LinkedList<Type>();
        }
        this.isRef = false;
    }


    @Override
    public String getTypeName() {
        String res = "";
        if(isRef){
            res = res.concat("(ref ");
        }
        res = res.concat("(");
        res = res.concat(type1.getTypeName()+","+type2.getTypeName());
        for(Type t: otherTypes){
            res = res.concat(","+t.getTypeName());
        }
        res =  res.concat(")");
        if(isRef){
            res = res.concat(") ");
        }
        return res;//TODO
    }

    @Override
    public void addRef() {

    }

    public List<Type> getOtherTypes() {
        return otherTypes;
    }

    public Type getType1() {
        return type1;
    }

    public Type getType2() {
        return type2;
    }

    public void setType1(Type type1) {
        this.type1 = type1;
    }

    public void setType2(Type type2) {
        this.type2 = type2;
    }

    public void setOtherTypes(List<Type> otherTypes) {
        this.otherTypes = otherTypes;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
