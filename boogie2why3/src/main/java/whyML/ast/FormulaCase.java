package whyML.ast;


import whyML.ast.formula.Formula;
import whyML.ast.pattern.Pattern;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FormulaCase implements ASTNode{
    private Pattern pattern;
    private Formula formula;


    public FormulaCase(Pattern pattern, Formula formula){
        this.pattern = pattern;
        this.formula = formula;
    }

    public Formula getFormula() {
        return formula;
    }

    public Pattern getPattern() {
        return pattern;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(pattern);
        children.add(formula);
        return children;
    }
}
