package whyML.ast.terms;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michael on 6/24/2015.
 */
public class TupleTerm extends Term {

    private List<Term> terms; //must contain at least 2

    public TupleTerm(List<Term> terms) {
        this.terms = terms;
    }

    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
