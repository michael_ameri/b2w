package whyML.ast.terms;


import whyML.ast.ASTNode;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FunctionApplicationWithTypeTerm extends Term {

    private String lqualid;
    //at least 1 term according to syntax.
    private List<Term> terms;

    private Type declaredType;

    public FunctionApplicationWithTypeTerm(String lqualid, List<Term> terms, Type declaredType){
        this.lqualid = lqualid;
        this.terms = terms;
        this.declaredType = declaredType;
    }

    public FunctionApplicationWithTypeTerm(String lqualid, Term term, Type declaredType){
        this.lqualid = lqualid;
        this.terms = new LinkedList<>();
        terms.add(term);
        this.declaredType = declaredType;
    }

        public List<Term> getTerms() {
        return terms;
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    public Type getDeclaredType() {
        return declaredType;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
