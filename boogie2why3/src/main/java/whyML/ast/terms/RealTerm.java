package whyML.ast.terms;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class RealTerm extends Term {
    //string representation of the real's value. Allows arbitrary precision, and we don't need computation.
    private String value;

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }

    public RealTerm(String value){
        this.value = value;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getValue() {
        return value;
    }


}
