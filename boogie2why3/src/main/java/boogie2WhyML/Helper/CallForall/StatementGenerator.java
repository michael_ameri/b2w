package boogie2WhyML.Helper.CallForall;


import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import boogie.enums.BinaryOperator;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StatementGenerator implements ASTVisitor{


   // private List<RequiresSpecification> preconditions;
    //private List<EnsuresSpecification> postconditions;
    private Map<String,Expression> parameterReplacementExpression;
    private List<VarList> boundVariables;


    public StatementGenerator() {
    }


    /**
     * modifies the formulas of the pre- and postconditions to use the new Expressions given by parameterReplacementExpression.
     * @param preconditions
     * @param postconditions
     * @param parameterReplacementExpression
     * @param boundVariables variables which are bound by the forall statement (one for each wildcard expression)
     * @return
     */
    public AssumeStatement generateEquivalentStatement(List<RequiresSpecification> preconditions,
                                                       List<EnsuresSpecification> postconditions,
                                                       Map<String, Expression> parameterReplacementExpression,
                                                       List<VarList> boundVariables){

        //this.preconditions = preconditions;
        //this.postconditions = postconditions;
        this.parameterReplacementExpression = parameterReplacementExpression;
        this.boundVariables = boundVariables;



        //first clone expressions so we don't alter the expressions in the pre and post conditions (only want to create
        //new ones for the assume statement).
        List<Expression> preExpressions = new LinkedList<>();
        for(RequiresSpecification r: preconditions){
            preExpressions.add(r.getFormula().clone());
        }

        List<Expression> postExpressions = new LinkedList<>();
        for(EnsuresSpecification e: postconditions){
            postExpressions.add(e.getFormula().clone());
        }

        for(Expression e: preExpressions){
            e.accept(this);
        }
        for(Expression e: postExpressions){
            e.accept(this);
        }


        Expression implicator = new BooleanLiteral(null,true);
        for(Expression e: preExpressions){
            implicator = new BinaryExpression(null, BinaryOperator.LOGICAND,implicator,e);
        }

        Expression implicatee = new BooleanLiteral(null,true);
        for(Expression e: postExpressions){
            implicatee = new BinaryExpression(null,BinaryOperator.LOGICAND,implicatee,e);
        }

        Expression implication = new BinaryExpression(null,BinaryOperator.LOGICIMPLIES,implicator,implicatee);
        if(boundVariables.isEmpty()){
            return new AssumeStatement(null,implication);
        }else{
            //create forall
            QuantifierExpression quantifierExpression = new QuantifierExpression(null,true,new String[0],boundVariables.toArray(new VarList[boundVariables.size()]),new Attribute[0],implication);
            return new AssumeStatement(null,quantifierExpression);
        }

    }


    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }


    /**
     * if expression is an identifier expression which needs to be replaced, the replacement is returned, otherwise
     * expression is visited and returned.
     * @param expression
     * @return
     */
    private Expression replaceExpressionIfNecessary(Expression expression){
        if(expression instanceof IdentifierExpression){
            return replaceIdentifierExpression((IdentifierExpression)expression);
        }else {
            expression.accept(this);
            return expression;
        }
    }

    /**
     * if identifierExpression has a replacement in parameterReplacementExpression, that is returned, otherwise identifierExpression
     * is returned.
     * Should always call replaceExpressionIfNecessary instead of this directly.
     * @param identifierExpression
     * @return
     */
    private Expression replaceIdentifierExpression(IdentifierExpression identifierExpression){
        Expression replacement = parameterReplacementExpression.get(identifierExpression.getIdentifier());
        if(replacement != null){
            return replacement;
        }else {
            return identifierExpression;
        }
    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        arrayAccessExpression.setArray(replaceExpressionIfNecessary(arrayAccessExpression.getArray()));
        for(int i = 0; i< arrayAccessExpression.getIndices().length; ++i){
            arrayAccessExpression.getIndices()[i] = replaceExpressionIfNecessary(arrayAccessExpression.getIndices()[i]);
        }
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

        arrayStoreExpression.setArray(replaceExpressionIfNecessary(arrayStoreExpression.getArray()));
        for(int i = 0; i < arrayStoreExpression.getIndices().length; ++i){
            arrayStoreExpression.getIndices()[i] = replaceExpressionIfNecessary(arrayStoreExpression.getIndices()[i]);
        }
        arrayStoreExpression.setValue(replaceExpressionIfNecessary(arrayStoreExpression.getValue()));

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        binaryExpression.setLeft(replaceExpressionIfNecessary(binaryExpression.getLeft()));
        binaryExpression.setRight(replaceExpressionIfNecessary(binaryExpression.getRight()));
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        bitVectorAccessExpression.setBitvec(replaceExpressionIfNecessary(bitVectorAccessExpression.getBitvec()));
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        //nothing.
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        for(int i = 0; i < functionApplication.getArguments().length; ++i){
            functionApplication.getArguments()[i] = replaceExpressionIfNecessary(functionApplication.getArguments()[i]);
        }
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        for(int i = 0; i < functionApplicationWithType.getArguments().length; ++i){
            functionApplicationWithType.getArguments()[i] = replaceExpressionIfNecessary(functionApplicationWithType.getArguments()[i]);
        }
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        //nothing
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        ifThenElseExpression.setCondition(replaceExpressionIfNecessary(ifThenElseExpression.getCondition()));
        ifThenElseExpression.setThenPart(replaceExpressionIfNecessary(ifThenElseExpression.getThenPart()));
        ifThenElseExpression.setElsePart(replaceExpressionIfNecessary(ifThenElseExpression.getElsePart()));
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        quantifierExpression.setSubformula(replaceExpressionIfNecessary(quantifierExpression.getSubformula()));
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        lambdaExpression.setSubformula(replaceExpressionIfNecessary(lambdaExpression.getSubExpression()));
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        unaryExpression.setExpr(replaceExpressionIfNecessary(unaryExpression.getExpr()));
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        //nothing
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
