package boogie2WhyML.Helper.CallForall;

import boogie.ast.VarList;
import boogie.ast.asttypes.ASTType;
import boogie.ast.expression.Expression;
import boogie.ast.expression.IdentifierExpression;
import boogie.ast.expression.WildcardExpression;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.specification.Specification;
import boogie.ast.statement.AssumeStatement;
import boogie.ast.statement.CallStatement;

import java.util.*;

/**
 * Used to store information about procedures which can be used in a call-forall statement.
 */
public class CallForallInfo {

    private List<RequiresSpecification> preconditions;
    private List<EnsuresSpecification> postconditons;
    private List<VarList> inputArguments;

    /**
     * automatically separates input arguments.
     * @param inputArguments
     * @param preconditions
     * @param postconditons
     */
    public CallForallInfo(List<VarList> inputArguments, List<RequiresSpecification> preconditions, List<EnsuresSpecification> postconditons) {
        this.inputArguments = separateVarlist(inputArguments);
        this.preconditions = preconditions;
        this.postconditons = postconditons;
    }

    /**
     * automatically separates input arguments. Specifications are split into pre- and postconditions. Other specifications (such as modifies clause) are ignored.
     * @param inputArguments
     * @param specifications
     */
    public CallForallInfo(VarList[] inputArguments, Specification[] specifications){
        List<RequiresSpecification> preconditions = new LinkedList<>();
        List<EnsuresSpecification> postconditions = new LinkedList<>();
        for(Specification specification: specifications){
            if(specification instanceof RequiresSpecification){
                preconditions.add((RequiresSpecification)specification);
            }else if(specification instanceof EnsuresSpecification){
                postconditions.add((EnsuresSpecification)specification);
            }
        }
        this.inputArguments = separateVarlist(Arrays.asList(inputArguments));
        this.preconditions = preconditions;
        this.postconditons = postconditions;
    }

    public List<RequiresSpecification> getPreconditions() {
        return preconditions;
    }

    public List<EnsuresSpecification> getPostconditons() {
        return postconditons;
    }

    /**
     * inputArguments are separated when constructor is called.
     * @return
     */
    public List<VarList> getInputArguments() {
        return inputArguments;
    }

    /**
     * separates varList of the form x,y:int into two varlists of the form x:int; y:int (keeps order intact)
     * null input returns null. If the varlist has no identifier, it returns null.
     * @param varList to be separated.
     * @return
     */
    public static List<VarList> separateVarlist(VarList varList){
        if (varList == null){
            return null;
        }
        if  (varList.getIdentifiers().length == 0){
            return null;
        }
        List<VarList> result = new LinkedList<>();
        for(String ident: varList.getIdentifiers()){
            String[] idents = {ident};
            result.add(new VarList(varList.getLocation(),idents,varList.getType()));
        }

        return result;
    }

    /**
     * separates varLists of the form x,y:int into two varlists of the form x:int; y:int
     * null input returns null. All varlists must have at least 1 identifier, otherwise null is returned.
     * @param varLists to be separated.
     * @return
     */
    public static List<VarList> separateVarlist(List<VarList> varLists){
        if(varLists == null){
            return null;
        }
        List<VarList> result = new LinkedList<>();
        for(VarList varList: varLists){
            if(varList.getIdentifiers().length == 0){
                return null;
            }
            result.addAll(separateVarlist(varList));
        }
        return result;
    }

    public static String boundVariablePrefix = "a_";//todo move to new class together with all prefixes

    /**
     *  returns an assume statement which is equivalent to callForallStatement. CallForallInfo can be obtained from CallForallInfoVisitor
     *  If the statement is not call-forall (i.e. if it is a simple call statement), then this method returns null.
     * @param callForallStatement
     * @param callForallInfo
     * @return an assumeStatement which is semantically equivalent to the callForallStatement.
     */
    public static AssumeStatement callForallReplacement(CallStatement callForallStatement, CallForallInfo callForallInfo){

        if(!callForallStatement.isForall()){
            return null;
        }

        //for each of the input parameters, this tells us by which expression it has been called with. e.g.
        //procedure lemma(x:int, y:bool)...
        //call forall lemma(3,z);
        //map: [x -> 3; y -> z]
        Map<String,Expression> parameterReplacementExpression = new LinkedHashMap<>();
        //at which positions (starting at 0) does the call-forall statement have wildcard expressions.
        //List<Integer> wildCardPositions = new LinkedList<>();
        List<VarList> boundVariables = new LinkedList<>();
        for(int i = 0; i < callForallStatement.getArguments().length; ++i){
            //we can assume that callForallInfo splits the input arguments into separate varLists.
            String currVarName = callForallInfo.getInputArguments().get(i).getIdentifiers()[0];
            ASTType currVarType = callForallInfo.getInputArguments().get(i).getType();
            Expression currReplacementExpression = callForallStatement.getArguments()[i];

            if(currReplacementExpression instanceof WildcardExpression){
                //we need to add it to bound variables, and give identifier expression as replacement.
                //wildCardPositions.add(i);
                IdentifierExpression replacement = new IdentifierExpression(null,boundVariablePrefix+currVarName);
                parameterReplacementExpression.put(currVarName,replacement);
                String[] varListName = {replacement.getIdentifier()};
                boundVariables.add(new VarList(null,varListName,currVarType));
            }else{
                //all other expressions serve directly as a replacement.
                parameterReplacementExpression.put(currVarName,currReplacementExpression);
            }
        }

        //call Statement generator to do the rest..
        StatementGenerator statementGenerator =  new StatementGenerator();
        return statementGenerator.generateEquivalentStatement(callForallInfo.preconditions,callForallInfo.postconditons,parameterReplacementExpression,boundVariables);
    }


}
