package boogie2WhyML.Helper.CallForall;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;

import java.util.HashMap;
import java.util.Map;

/**
 * replaces call forall statements by equivalent assume statements in the boogie AST.
 */
public class CallForallReplacementVisitor implements ASTVisitor{

    //map from procedure name to infos needed for call forall translation.
    private Map<String,CallForallInfo> procedureInfos;

    public CallForallReplacementVisitor(ProgramFactory programFactory){
        procedureInfos = new HashMap<>();
        //first fill out info about procedures which can be used as call forall.
        for(Declaration d: programFactory.getASTRootWithoutModfiesClause().getDeclarations()){
            if(d instanceof ProcedureDeclaration){
                ProcedureDeclaration procedureDeclaration = (ProcedureDeclaration)d;
                //we could instead only add if it matches criteria of possible call forall candidate, such as no output params. this is not necessary, but perhaps slightly more efficient.
                CallForallInfo callForallInfo = new CallForallInfo(procedureDeclaration.getInParams(),procedureDeclaration.getSpecification());
                procedureInfos.put(procedureDeclaration.getIdentifier(),callForallInfo);
            }
        }


        //now visit all and replace call foralls.
        for(Declaration d: programFactory.getASTRootWithoutModfiesClause().getDeclarations()){
            d.accept(this);
        }


    }



    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {
        //we replace it directly in parent, we don't visit it.
    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {
        for(int i = 0; i < ifStatement.getThenPart().length; ++i){
            Statement s = ifStatement.getThenPart()[i];
            if(s instanceof CallStatement && ((CallStatement) s).isForall()){
                ifStatement.getThenPart()[i] = CallForallInfo.callForallReplacement((CallStatement)s,procedureInfos.get(((CallStatement) s).getMethodName()));
            }
        }
        for(int i = 0; i < ifStatement.getElsePart().length; ++i){
            Statement s = ifStatement.getElsePart()[i];
            if(s instanceof CallStatement && ((CallStatement) s).isForall()){
                ifStatement.getElsePart()[i] = CallForallInfo.callForallReplacement((CallStatement)s,procedureInfos.get(((CallStatement) s).getMethodName()));
            }
        }
    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {
        for(int i = 0; i < whileStatement.getBody().length; ++i){
            Statement s = whileStatement.getBody()[i];
            if(s instanceof CallStatement && ((CallStatement) s).isForall()){
                whileStatement.getBody()[i] = CallForallInfo.callForallReplacement((CallStatement)s,procedureInfos.get(((CallStatement) s).getMethodName()));
            }
        }
    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {
        for(int i = 0; i < body.getBlock().length; ++i){
            Statement s = body.getBlock()[i];
            if(s instanceof CallStatement && ((CallStatement) s).isForall()){
                body.getBlock()[i] = CallForallInfo.callForallReplacement((CallStatement)s,procedureInfos.get(((CallStatement) s).getMethodName()));
            }
        }
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {
        implementation.getBody().accept(this);
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        if(procedureDeclaration.getBody() != null){
            procedureDeclaration.getBody().accept(this);
        }
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
