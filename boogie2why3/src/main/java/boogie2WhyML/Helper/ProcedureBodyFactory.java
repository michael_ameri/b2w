package boogie2WhyML.Helper;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import javafx.util.Pair;
import util.Log;

import java.util.*;

/**
 * In Boogie, procedure and and implementation can be separate.
 * Implementation can change the names of input arguments.
 * Implementation takes whereClauses from Procedure (and cannot define new ones)
 * Implementation can rename and reorder type arguments
 * This class renames implementation input and output idents to the same as those of procedures.
 * And for each procedure, it creates a list of bodies from different implementations.
 */
public class ProcedureBodyFactory implements ASTVisitor{

    private ProgramFactory programFactory;

    //map from procedure name, to its declaration
    private Map<String,ProcedureDeclaration> procedureDeclarationMap;

    private Map<String,List<Body>> procedureBodies;

    //map from procedure name to map from pair of String[], where first are the names of in params, then the names of out params.
    private Map<String,String[]> procedureInputNames;
    private Map<String,String[]> procedureOutputNames;

    private Set<ASTNode> processed = new HashSet<>();

    /**
     * process children of an ASTNode
     * @param objects
     */
    private void process(Collection<Object> objects){
        for(Object o: objects){
            if(o instanceof ASTNode){
                ASTNode node = (ASTNode)o;
                if(!processed.contains(node)){
                    node.accept(this);
                    processed.add(node);
                }
            }else if(o instanceof Collection){
                process((Collection)o);
            }else if(o instanceof Object[]){
                //this works b/c Arrays.asList keep same object references.
                //from java doc: (Changes to the returned list "write through" to the array.)
                process(Arrays.asList((Object[]) o));
            }
        }
    }


    /**
     * if it is not null, we return the name from the procedure. otherwise we return input itself.
     * @param input
     * @return
     */
    private String getProcedureNamePendant(String input){
        if(procedureNamePendant.get(input) != null){
            return procedureNamePendant.get(input);
        }else {
            return input;
        }
    }



    public Map<String,List<Body>> modifyProcedureDeclarations(ProgramFactory programFactory){
        this.programFactory = programFactory;
        this.procedureDeclarationMap = new HashMap<>();
        this.procedureBodies = new HashMap<>();
        this.processed = new HashSet<>();
        procedureInputNames = new HashMap<>();
        procedureOutputNames = new HashMap<>();

        List<Declaration> declarations = programFactory.getGlobalDeclarations();
        for(Declaration d: declarations){
            if(d instanceof ProcedureDeclaration){
                ProcedureDeclaration procedureDeclaration = (ProcedureDeclaration)d;
                procedureDeclarationMap.put(procedureDeclaration.getIdentifier(), procedureDeclaration);
                List<Body> procedureBody = new LinkedList<>();
                if(procedureDeclaration.getBody() != null){
                    procedureBody.add(procedureDeclaration.getBody());
                }
                procedureBodies.put(procedureDeclaration.getIdentifier(),procedureBody);

                List<String> inputParameterIdents = new LinkedList<>();
                for(VarList inParams:procedureDeclaration.getInParams()){
                    inputParameterIdents.addAll(Arrays.asList(inParams.getIdentifiers()));
                }

                List<String> outputParameterIdents = new LinkedList<>();
                for(VarList outParams:procedureDeclaration.getOutParams()){
                    outputParameterIdents.addAll(Arrays.asList(outParams.getIdentifiers()));
                }

                procedureInputNames.put(procedureDeclaration.getIdentifier(),inputParameterIdents.toArray(new String[inputParameterIdents.size()]));
                procedureOutputNames.put(procedureDeclaration.getIdentifier(),outputParameterIdents.toArray(new String[outputParameterIdents.size()]));
            }
        }
        for(Declaration d: declarations){
            if(d instanceof Implementation){
                d.accept(this);
            }

        }
        return procedureBodies;
    }



    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        //for attributes.
    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        process(arrayAccessExpression.getChildren());
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        process(arrayStoreExpression.getChildren());
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        process(binaryExpression.getChildren());
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        process(bitVectorAccessExpression.getChildren());
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        throw new RuntimeException("Error: codeExpression not supported yet");
        //process(codeExpression.getChildren());
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        process(functionApplication.getChildren());
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        process(functionApplicationWithType.getChildren());
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        //We want to replace the identifier expression, but only if we have to. (e.g. for call expressions, we don't have to.
        identifierExpression.setIdentifier(getProcedureNamePendant(identifierExpression.getIdentifier()));
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        process(ifThenElseExpression.getChildren());
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        process(quantifierExpression.getChildren());
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        process(lambdaExpression.getChildren());
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        process(unaryExpression.getChildren());
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        process(wildcardExpression.getChildren());
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        throw new RuntimeException("Error: shouldn't get here");
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        process(loopInvariantSpecification.getChildren());
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        throw new RuntimeException("Error: shouldn't get here");
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        throw new RuntimeException("Error: shouldn't get here");
        //process(requiresSpecification.getChildren());
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        process(assertStatement.getChildren());
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        process(assignmentStatement.getChildren());
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        process(assumeStatement.getChildren());
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        process(breakStatement.getChildren());
    }

    @Override
    public void visit(CallStatement callStatement) {
        String[] oldLHS = callStatement.getLhs();
        String[] newLHS = new String[oldLHS.length];
        for(int i=0;i<oldLHS.length;++i){
            newLHS[i] = getProcedureNamePendant(oldLHS[i]);
        }
        callStatement.setLhs(newLHS);
        process(callStatement.getChildren());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        process(gotoStatement.getChildren());
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        String[] oldIdents = havocStatement.getIdentifiers();
        String[] newIdents = new String[oldIdents.length];
        for(int i=0;i<oldIdents.length;++i){
            newIdents[i] = getProcedureNamePendant(oldIdents[i]);
        }
        havocStatement.setIdentifiers(newIdents);
        process(havocStatement.getChildren());
    }

    @Override
    public void visit(IfStatement ifStatement) {
        process(ifStatement.getChildren());
    }

    @Override
    public void visit(Label label) {
        //nothing.
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        process(parallelCall.getChildren());
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        process(returnStatement.getChildren());
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        process(whileStatement.getChildren());
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        Log.error("procedure body factory yield statement");
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        for(Expression e : arrayLHS.getIndices()){
            e.accept(this);
        }
        arrayLHS.getArray().accept(this);
    }

    @Override
    public void visit(Body body) {
        process(body.getChildren());
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {
        //nothin
    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {
        //cannot be part of implementation.
    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {
        variableLHS.setIdentifier(getProcedureNamePendant(variableLHS.getIdentifier()));
    }



    @Override
    public void visit(VarList varList) {
        if(varList.getWhereClause() != null){
            varList.getWhereClause().accept(this);
        }
        String[] newIdents = new String[varList.getIdentifiers().length];
        for(int i = 0; i < varList.getIdentifiers().length; ++i){
            newIdents[i] = getProcedureNamePendant(varList.getIdentifiers()[i]);
            if(newIdents[i] == null){
                throw new RuntimeException("Error: something went wrong.");
            }
        }
        varList.setIdentifiers(newIdents);
    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }


    //map from parameter name in implementation, to corresponding parameter name in procedure.
    /**
     * do NOT use procedureNamePendant.get(..), always use getProcdureNamePendant.
     */
    private Map<String,String> procedureNamePendant;

    @Override
    public void visit(Implementation implementation) {
        //currentImplementationName = implementation.getIdentifier();
        procedureNamePendant = new HashMap<>();
        int offset = 0;
        for(VarList varList: implementation.getInParams()){
            //varList.accept(this);
            String[] newVarlistIdents = new String[varList.getIdentifiers().length];

            for(int i = 0; i < varList.getIdentifiers().length; ++i){
                String implementationVarName = varList.getIdentifiers()[i];
                String procedureVarName = procedureInputNames.get(implementation.getIdentifier())[i+offset];
                procedureNamePendant.put(implementationVarName,procedureVarName);
                newVarlistIdents[i] = procedureVarName;
            }
            offset = offset + varList.getIdentifiers().length;
            //varList.setIdentifiers(procedureInputNames.get(implementation.getIdentifier()));
            varList.setIdentifiers(newVarlistIdents);
        }
        offset = 0;
        for(VarList varList: implementation.getOutParams()){
            //varList.accept(this);
            String[] newVarListIdents = new String[varList.getIdentifiers().length];
            for(int i = 0; i < varList.getIdentifiers().length; ++i){
                String implementationVarName = varList.getIdentifiers()[i];
                String procedureVarName = procedureOutputNames.get(implementation.getIdentifier())[i+offset];
                procedureNamePendant.put(implementationVarName,procedureVarName);
                newVarListIdents[i] = procedureVarName;
            }
            offset = offset + varList.getIdentifiers().length;
            //varList.setIdentifiers(procedureOutputNames.get(implementation.getIdentifier()));
            varList.setIdentifiers(newVarListIdents);
        }
        implementation.getBody().accept(this);
        procedureBodies.get(implementation.getIdentifier()).add(implementation.getBody());
        //currentImplementationName = null;
        procedureNamePendant = null;
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        for(VarList varList: variableDeclaration.getVariables()){
            varList.accept(this);
        }
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
