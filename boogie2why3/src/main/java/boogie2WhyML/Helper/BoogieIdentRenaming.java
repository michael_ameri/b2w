package boogie2WhyML.Helper;

import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import util.Log;

import java.util.*;

/**
 * Rename identifiers in a Boogie AST with identifiers which are allowed by WhyML (and also Boogie, since it is a subset.)
 */
public class BoogieIdentRenaming implements ASTVisitor {

    public void replaceIdents(List<Declaration> declarations){
        for (Declaration d : declarations){
            d.accept(this);
        }
    }

    /*
    helper methods
    */

    private String lprefix = "__";


    /**
     * contains names of input arguments, output arguments, and local variables.
     * Must be set by procedure-, function- and implementation declarations.
     */
    private Set<String> possiblyShadowed = new HashSet<>();


    //does not change upperscore beginning.
    private String uprefix = "";


    /**
     * replaces unsupported characters, and makes the identifier lower case.
     * @param input
     * @return
     */
    private String lRenameAndReplace(String input){
        if(noReplacingAllowed(input)){
            return input;
        }
        String res = lprefix + replaceUnsupportedChars(input);
        //if this ident is possibly a shadow, we make it unique by giving it a different prefix than globally defined declarations
        if(possiblyShadowed.contains(input)){
            res = lprefix + res;
        }
        return res;
    }

    private String uRenameAndReplace(String input) { return uprefix+replaceUnsupportedChars(input); }


    private boolean noReplacingAllowed(String s){
        return s.trim().equals("int") || s.trim().equals("bool") || s.trim().equals("real")
                || s.trim().equals("true")|| s.trim().equals("false");
    }


    /**
     * replaces character which are allowed in Boogie, but not in WhyML, with some that are allowed in both.
     * @param input
     * @return
     */
    private String replaceUnsupportedChars(String input){
        //TODO unicode characters...
        String res = input;
        res = res.replace("_", "_US_");
        res = res.replace("#", "_PD_");
        res = res.replace("$","_DL_");
        res = res.replace(".","_DOT_");
        res = res.replace("?","_SQ_");
        res = res.replace("`","_BQ_");
        res = res.replace("~","_TD_");
        res = res.replace("^","_CT_");
        res = res.replace("\\","_BS_");
        res = res.replace("?","_QM_");
        return res;
    }

    private Set<ASTNode> processed = new HashSet<>();

    /**
     * process children of an AST node.
     * @param objects
     */
    private void process(Collection<Object> objects){
        for(Object o: objects){
            if(o instanceof ASTNode){
                ASTNode node = (ASTNode)o;
                if(!processed.contains(node)){
                    node.accept(this);
                    processed.add(node);
                }
            }else if(o instanceof Collection){
                process((Collection)o);
            }else if(o instanceof Object[]){
                //(Changes to the returned list "write through" to the array according to javadoc, so this works)
                process(Arrays.asList((Object[]) o));
            }
        }
    }


    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        process(bitvecLiteral.getChildren());
    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        process(booleanLiteral.getChildren());
    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {
        process(integerLiteral.getChildren());
    }

    @Override
    public void visit(RealLiteral realLiteral) {
        process(realLiteral.getChildren());
    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        //String literal are used in attributes only, so we probably don't want to rename them.
        //Log.info("String literal not renamed: "+stringLiteral);
    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        process(arrayAccessExpression.getChildren());
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        process(arrayStoreExpression.getChildren());
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        process(binaryExpression.getChildren());
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        process(bitVectorAccessExpression.getChildren());
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        process(codeExpression.getChildren());
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        functionApplication.setIdentifier(lRenameAndReplace(functionApplication.getIdentifier()));
        process(functionApplication.getChildren());
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        functionApplicationWithType.setIdentifier(lRenameAndReplace(functionApplicationWithType.getIdentifier()));
        process(functionApplicationWithType.getChildren());
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        identifierExpression.setIdentifier(lRenameAndReplace(identifierExpression.getIdentifier()));
        process(identifierExpression.getChildren());
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        process(ifThenElseExpression.getChildren());
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        Set<String> oiriginalPossiblyShadowed = new HashSet<>(possiblyShadowed);
        String[] oldTypeParams = quantifierExpression.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        quantifierExpression.setTypeParams(newTypeParams);

        VarList[] parameters = quantifierExpression.getParameters();
        for(VarList v: parameters){
            for(String s : v.getIdentifiers()){
                possiblyShadowed.add(s);
            }
        }
        process(quantifierExpression.getChildren());
        possiblyShadowed = new HashSet<>(oiriginalPossiblyShadowed);
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        String[] oldTypeParams = lambdaExpression.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        lambdaExpression.setTypeParams(newTypeParams);
        process(lambdaExpression.getChildren());
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        process(unaryExpression.getChildren());
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        process(wildcardExpression.getChildren());
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        process(ensuresSpecification.getChildren());
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        process(loopInvariantSpecification.getChildren());
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        String[] oldIdentifiers = modifiesSpecification.getIdentifiers();
        String[] newIdentifiers = new String[oldIdentifiers.length];
        for(int i = 0; i < oldIdentifiers.length; ++i){
            newIdentifiers[i] = lRenameAndReplace(oldIdentifiers[i]);
        }
        modifiesSpecification.setIdentifiers(newIdentifiers);
        process(modifiesSpecification.getChildren());
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        process(requiresSpecification.getChildren());
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        process(assertStatement.getChildren());
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        process(assignmentStatement.getChildren());
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        process(assumeStatement.getChildren());
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        process(breakStatement.getChildren());
    }

    @Override
    public void visit(CallStatement callStatement) {
        String[] oldIdentifiers = callStatement.getLhs();
        String[] newIdentifiers = new String[oldIdentifiers.length];
        for(int i = 0; i < oldIdentifiers.length; ++i){
            newIdentifiers[i] = lRenameAndReplace(oldIdentifiers[i]);
        }
        callStatement.setLhs(newIdentifiers);
        callStatement.setMethodName(lRenameAndReplace(callStatement.getMethodName()));
        process(callStatement.getChildren());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        process(gotoStatement.getChildren());
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        String[] oldIdentifiers = havocStatement.getIdentifiers();
        String[] newIdentifiers = new String[oldIdentifiers.length];
        for(int i = 0; i < oldIdentifiers.length; ++i){
            newIdentifiers[i] = lRenameAndReplace(oldIdentifiers[i]);
        }
        havocStatement.setIdentifiers(newIdentifiers);
        process(havocStatement.getChildren());
    }

    @Override
    public void visit(IfStatement ifStatement) {
        process(ifStatement.getChildren());
    }

    @Override
    public void visit(Label label) {
        process(label.getChildren());
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        process(parallelCall.getChildren());
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        process(returnStatement.getChildren());
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        process(whileStatement.getChildren());
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        process(yieldStatement.getChildren());
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        process(arrayLHS.getChildren());
    }

    @Override
    public void visit(Body body) {
        process(body.getChildren());
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {
        namedAttribute.setName(lRenameAndReplace(namedAttribute.getName()));
        process(namedAttribute.getChildren());
    }

    @Override
    public void visit(ParentEdge parentEdge) {
        parentEdge.setIdentifier(lRenameAndReplace(parentEdge.getIdentifier()));
        process(parentEdge.getChildren());
    }

    @Override
    public void visit(Project project) {
        process(project.getChildren());
    }

    @Override
    public void visit(Trigger trigger) {
        process(trigger.getChildren());
    }

    @Override
    public void visit(Unit unit) {
        process(unit.getChildren());
    }

    @Override
    public void visit(VariableLHS variableLHS) {
        variableLHS.setIdentifier(lRenameAndReplace(variableLHS.getIdentifier()));
        process(variableLHS.getChildren());
    }

    @Override
    public void visit(VarList varList) {
        String[] oldIdentifiers = varList.getIdentifiers();
        String[] newIdentifiers = new String[oldIdentifiers.length];
        for(int i = 0; i < oldIdentifiers.length; ++i){
            newIdentifiers[i] = lRenameAndReplace(oldIdentifiers[i]);
        }
        varList.setIdentifiers(newIdentifiers);
        process(varList.getChildren());
    }

    @Override
    public void visit(Axiom axiom) {
        process(axiom.getChildren());
    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {
        process(constDeclaration.getChildren());
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

        possiblyShadowed = new HashSet<>();
        List<VarList> localPossiblyShadowed = new LinkedList<>();
        localPossiblyShadowed.addAll(Arrays.asList(functionDeclaration.getInParams()));
        localPossiblyShadowed.add(functionDeclaration.getOutParam());
        for(VarList varList: localPossiblyShadowed){
            for(String ident : varList.getIdentifiers()){
                possiblyShadowed.add(ident);
            }
        }
        functionDeclaration.setIdentifier(lRenameAndReplace(functionDeclaration.getIdentifier()));
        String[] oldTypeParams = functionDeclaration.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        functionDeclaration.setTypeParams(newTypeParams);
        process(functionDeclaration.getChildren());
        possiblyShadowed = new HashSet<>();
    }

    @Override
    public void visit(Implementation implementation) {
        possiblyShadowed = new HashSet<>();
        List<VarList> localPossiblyShadowed = new LinkedList<>();
        localPossiblyShadowed.addAll(Arrays.asList(implementation.getInParams()));
        localPossiblyShadowed.addAll(Arrays.asList(implementation.getOutParams()));


        String[] oldTypeParams = implementation.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        implementation.setTypeParams(newTypeParams);


        if(implementation.getBody() != null){
            for(VariableDeclaration vd : implementation.getBody().getLocalVars()){
                localPossiblyShadowed.addAll(Arrays.asList(vd.getVariables()));
            }
        }
        for(VarList varList: localPossiblyShadowed){
            for(String ident : varList.getIdentifiers()){
                possiblyShadowed.add(ident);
            }
        }
        implementation.setIdentifier(lRenameAndReplace(implementation.getIdentifier()));
        process(implementation.getChildren());
        possiblyShadowed = new HashSet<>();
    }


    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        possiblyShadowed = new HashSet<>();
        String[] oldTypeParams = procedureDeclaration.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        procedureDeclaration.setTypeParams(newTypeParams);

        List<VarList> localPossiblyShadowed = new LinkedList<>();
        localPossiblyShadowed.addAll(Arrays.asList(procedureDeclaration.getInParams()));
        localPossiblyShadowed.addAll(Arrays.asList(procedureDeclaration.getOutParams()));
        if(procedureDeclaration.getBody() != null){
            for(VariableDeclaration vd : procedureDeclaration.getBody().getLocalVars()){
                localPossiblyShadowed.addAll(Arrays.asList(vd.getVariables()));
            }
        }
        for(VarList varList: localPossiblyShadowed){
            for(String ident : varList.getIdentifiers()){
                possiblyShadowed.add(ident);
            }
        }
        procedureDeclaration.setIdentifier(lRenameAndReplace(procedureDeclaration.getIdentifier()));
        process(procedureDeclaration.getChildren());
        possiblyShadowed = new HashSet<>();
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        typeDeclaration.setIdentifier(lRenameAndReplace(typeDeclaration.getIdentifier()));
        String[] oldTypeParams = typeDeclaration.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        typeDeclaration.setTypeParams(newTypeParams);
        process(typeDeclaration.getChildren());
    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        process(variableDeclaration.getChildren());
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {
        String[] oldTypeParams = arrayAstType.getTypeParams();
        String[] newTypeParams = new String[oldTypeParams.length];
        for(int i = 0; i < oldTypeParams.length; ++i){
            newTypeParams[i] = lRenameAndReplace(oldTypeParams[i]);
        }
        arrayAstType.setTypeParams(newTypeParams);
        process(arrayAstType.getChildren());
    }

    @Override
    public void visit(NamedAstType namedAstType) {
        namedAstType.setName(lRenameAndReplace(namedAstType.getName()));
        process(namedAstType.getChildren());
    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        //dont modify names..
        process(primitiveAstType.getChildren());
    }
}
