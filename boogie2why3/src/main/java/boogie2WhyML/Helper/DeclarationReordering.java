package boogie2WhyML.Helper;


import boogie.ast.specification.RequiresSpecification;
import whyML.ast.declerations.*;
import whyML.ast.declerations.letdeclaration.LetDeclaration;
import whyML.ast.declerations.letdeclaration.RecursiveLetDeclaration;

import java.util.LinkedList;
import java.util.List;

public class DeclarationReordering {

    private List<Declaration> firstInputDeclarations;
    private List<Declaration> middleInputDeclarations;
    private List<Declaration> lastInputDeclarations;
    private List<Declaration> orderedDeclarations = new LinkedList<>();

    /**
     * Returns a list with elements of the three inputs, where middleInputDelcarations are ordered according to spec.
     * @param firstInputDeclarations
     * @param middleInputDeclarations will be ordered according to spec.
     * @param lastInputDeclarations
     */
    public DeclarationReordering(List<Declaration> firstInputDeclarations, List<Declaration> middleInputDeclarations,
                                 List<Declaration> lastInputDeclarations) {
        this.firstInputDeclarations = firstInputDeclarations;
        this.middleInputDeclarations = middleInputDeclarations;
        this.lastInputDeclarations = lastInputDeclarations;
    }



    /**
     * Declarations are ordered according to:
     * 0. Use Declarations / Exception Declaration
     * 1. type Declarations
     * 2. global variable Declarations
     * 3. functions / Predicates
     * 4. axioms
     * 5. val abstract functions
     * 6. let declarations
     * 7. (rest)
     */
    private void orderDeclarations(){
        orderedDeclarations = new LinkedList<>();

        List<Declaration> axioms = new LinkedList<>();
        List<Declaration> comments = new LinkedList<>();
        List<Declaration> constants = new LinkedList<>();
        List<Declaration> emptys = new LinkedList<>();
        List<Declaration> exceptions = new LinkedList<>();
        List<Declaration> functions = new LinkedList<>();
        List<Declaration> predicates = new LinkedList<>();
        List<Declaration> types = new LinkedList<>();
        List<Declaration> uses = new LinkedList<>();
        List<Declaration> abstractFunctions = new LinkedList<>();
        List<Declaration> globalVariables = new LinkedList<>();
        List<Declaration> recursiveLetDeclarations = new LinkedList<>();
        List<Declaration> letDeclarations = new LinkedList<>();


        for(Declaration d: middleInputDeclarations){
            if(d instanceof Axiom){
                axioms.add(d);
            }else if(d instanceof Comment){
                comments.add(d);
            }else if(d instanceof ConstantDeclaration){
                constants.add(d);
            }else if(d instanceof Empty){
                emptys.add(d);
            }else if(d instanceof ExceptionDeclaration){
                exceptions.add(d);
            }else if(d instanceof FunctionDeclaration){
                functions.add(d);
            }else if(d instanceof Predicate){
                predicates.add(d);
            }else if(d instanceof TypeDeclaration){
                types.add(d);
            }else if(d instanceof Use){
                uses.add(d);
            }else if(d instanceof ValAbstractFunction){
                abstractFunctions.add(d);
            }else if(d instanceof ValGlobalVariable){
                globalVariables.add(d);
            }else if(d instanceof RecursiveLetDeclaration){
                recursiveLetDeclarations.add(d);
            }else if(d instanceof LetDeclaration){
                letDeclarations.add(d);
            }else{
                throw new RuntimeException("Error: unknown declaration.");
            }
        }
        /**
         * Declarations are ordered according to:
         * 0. Use Declarations / Exception Declaration
         * 1. type Declarations
         * 2. a. constants
         *    b. global variable Declarations
         * 3. functions / Predicates
         * 4. axioms
         * 5. val abstract functions
         * 6. let declarations
         * 7. (rest)
         */

        orderedDeclarations.addAll(firstInputDeclarations);
        orderedDeclarations.addAll(uses);
        orderedDeclarations.addAll(exceptions);
        orderedDeclarations.addAll(types);
        orderedDeclarations.addAll(constants);
        orderedDeclarations.addAll(globalVariables);
        orderedDeclarations.addAll(functions);
        orderedDeclarations.addAll(predicates);
        orderedDeclarations.addAll(axioms);
        orderedDeclarations.addAll(abstractFunctions);
        orderedDeclarations.addAll(recursiveLetDeclarations);
        orderedDeclarations.addAll(letDeclarations);
        orderedDeclarations.addAll(emptys);
        orderedDeclarations.addAll(comments);
        orderedDeclarations.addAll(lastInputDeclarations);

    }

    public List<Declaration> getOrderedDeclarations() {
        orderDeclarations();
        return orderedDeclarations;
    }
}
