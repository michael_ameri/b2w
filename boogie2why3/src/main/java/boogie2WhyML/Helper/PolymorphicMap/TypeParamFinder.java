package boogie2WhyML.Helper.PolymorphicMap;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import boogie.type.*;
import boogie2WhyML.Helper.VariableTypeParam.VariableWithTypeParamFinder;
import util.Log;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Creates a map from every boogie map type with type parameters, to all concrete instantiations of those type parameters.
 * If it contains multiple type parameters, the it also maps to type where only one is instantiated. //TODO should this change?
 * //TODO for now, it only works if this is the outermost type. I.e. it cannot be e.g. (Set HeapType)
 */
public class TypeParamFinder implements ASTVisitor{

    //first part could also be ArrayType instead of BoogieType
    private HashMap<ArrayType,Set<ArrayInstantiationType>> typeParamInstatiations;

    public TypeParamFinder(ProgramFactory programFactory){
        typeParamInstatiations = new HashMap<>();
        List<Declaration> declarations = Arrays.asList(programFactory.getASTRootWithoutModfiesClause().getDeclarations());
        for(Declaration d: declarations){
            d.accept(this);
        }
        //need to go through AST twice, or in some rare cases some instantiations may be missed because we haven't found
        //parameterized type yet.
        for(Declaration d: declarations){
            d.accept(this);
        }

        //if one of them has no instantiations, use dummy type.
        for(Map.Entry<ArrayType, Set<ArrayInstantiationType>> entry: typeParamInstatiations.entrySet()){
            Set<ArrayInstantiationType> instantiations = entry.getValue();
            ArrayType arrayType = entry.getKey();
            if(instantiations.isEmpty()){

            }
        }
    }

    /**
     *
     * @return map from a
     */
    public HashMap<ArrayType,Set<ArrayInstantiationType>> getTypeParamInstatiations(){
        return typeParamInstatiations;
    }

    /**
     * returns true, iff this type contains a placeholder type, or is a map type with a type parameter.
     * also returns true e.g. for types type set a = [a]bool;
     * @param boogieType
     * @return
     */
    public static boolean containsTypeParams(BoogieType boogieType){
        BoogieType actualType = boogieType.getUnderlyingType();
        if(actualType instanceof ArrayType){
            //return true if this arrayType has params. otherwise check if one of index types of value type is a map type with type params.
            ArrayType arrayType = (ArrayType)actualType;
            if(arrayType.getNumPlaceholders() > 0){
                return true;
            }else{
                for(int i = 0; i < arrayType.getIndexCount(); ++i){
                    if(containsTypeParams(arrayType.getIndexType(i))){
                        return true;
                    }
                }
                return containsTypeParams(arrayType.getValueType());
            }
        }else if(actualType instanceof ConstructedType){
            ConstructedType constructedType = (ConstructedType)actualType;
            for(int i = 0; i < constructedType.getConstr().getParamCount(); ++i){
                if(containsTypeParams(constructedType.getParameter(i))){
                    return true;
                }
            }
            return false;
        }else if(actualType instanceof PlaceholderType){
            return true;
        }else if(actualType instanceof PrimitiveType){
            return false;
        }else{
            throw new RuntimeException("Error: unknown Boogie type");
        }
    }


    /**
     * returns true, iff boogieType is an ArrayType of the form <...,a,...>[...,a*?,...]...
     * @param boogieType
     * @return
     */
    private static boolean isArrayTypeWithTypeParam(BoogieType boogieType){
        return (boogieType instanceof ArrayType && ((ArrayType)boogieType).getNumPlaceholders() > 0);
    }


    /**
     *
     * @param subType
     * @param superType
     * @return true, iff subType is a possible instatiation of superType. If one of the types is null, it returns false.
     * If super type contains no type parameters, it returns true iff subType equals superType.
     */
    public static boolean isInstatiationOf(BoogieType subType, BoogieType superType){
        subType = subType.getUnderlyingType();
        superType = superType.getUnderlyingType();
        if(subType == null || superType == null){
            return false;
        }
        /*can't check for this! superType might be placeholder type, and subtype instantiation, e.g. primitive type.
        if(!(subType.getUnderlyingType().getClass().equals(superType.getUnderlyingType().getClass()))){
            return false;
        }
        */
        if(superType instanceof ArrayType){
            if(!(subType instanceof ArrayType)){
                return false;
            }
            ArrayType arraySubType = (ArrayType)subType;
            ArrayType arraySuperType = (ArrayType)superType;
            if(arraySubType.getIndexCount() != arraySuperType.getIndexCount()){
                return false;
            }
            int indexCount = arraySubType.getIndexCount();
            for(int i = 0; i < indexCount; ++i){
                if(!isInstatiationOf(arraySubType.getIndexType(i),arraySuperType.getIndexType(i))){
                    return false;
                }
            }
            return isInstatiationOf(arraySubType.getValueType(),arraySuperType.getValueType());
        }else if(superType instanceof PrimitiveType){
            return superType.equals(subType);
        }else if(superType instanceof PlaceholderType){
            if(!(subType instanceof  PlaceholderType)){
                //super type is a placeholder, but subtype is not, so it is an instantiation.
                return true;
            }else {
                //super type and sub type are both placeholders, so it is not an instantiation.
                return false;
            }
        }else if(superType instanceof ConstructedType){
            if(!(subType instanceof  ConstructedType)){
                return false;
            }
            ConstructedType constructedSubType = (ConstructedType)subType;
            ConstructedType constructedSuperType = (ConstructedType)superType;
            if(!constructedSubType.getConstr().getName().equals(constructedSuperType.getConstr().getName())){
                return false;
            }
            if(constructedSuperType.getConstr().getParamCount() != constructedSubType.getConstr().getParamCount()){
                return false;
            }
            int paramCount = constructedSubType.getConstr().getParamCount();
            for(int i = 0;i < paramCount;++i){
                //if(!(constructedSubType.getParameter(i).equals(constructedSuperType.getParameter(i)))){
                if(!(isInstatiationOf(constructedSubType.getParameter(i),constructedSuperType.getParameter(i)))){
                    return false;
                }
            }
            //TODO need to check for synonym? shouldn't be necessary since we always use underlying type.
            return (constructedSubType.getConstr().isFinite() == constructedSuperType.getConstr().isFinite());
        }else {
            throw new RuntimeException("Error: unknown Boogie type.");
        }
    }


    private Set<ASTNode> processed = new HashSet<>();

    /**
     * process children of an AST node.
     * @param objects
     */
    private void process(Collection<Object> objects){
        for(Object o: objects){
            if(o instanceof ASTNode){
                ASTNode node = (ASTNode)o;
                if(!processed.contains(node)){
                    node.accept(this);
                    processed.add(node);
                }
            }else if(o instanceof Collection){
                process((Collection)o);
            }else if(o instanceof Object[]){
                //(Changes to the returned list "write through" to the array according to javadoc, so this works)
                process(Arrays.asList((Object[]) o));
            }
        }
    }

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }



    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        BoogieType overallType = arrayAccessExpression.getArray().getType().getUnderlyingType();
        if(isArrayTypeWithTypeParam(overallType)){
            if(!(overallType instanceof ArrayType) || !(overallType.getUnderlyingType() instanceof ArrayType)){
                //Not sure if this is possible at all, since we are in an arrayAccessExpression.
                throw new RuntimeException("Error: array type with type variable is contained within another type: "+overallType);
            }
            ArrayType arrayType = (ArrayType)overallType.getUnderlyingType();
            if(typeParamInstatiations.get(overallType) == null){
                typeParamInstatiations.put(arrayType,new HashSet<>());
            }
            Set<ArrayInstantiationType> instantiations = typeParamInstatiations.get(overallType);
            List<BoogieType> parameterizedIndexTypes = new LinkedList<>();

            boolean isInstantiation = true;
            List<BoogieType> instantiatedIndexTypes = Arrays.asList(arrayAccessExpression.getIndices()).stream().map(expression -> expression.getType().getUnderlyingType()).collect(Collectors.toList());
            for(int i = 0; i < arrayType.getIndexCount(); ++i){
                parameterizedIndexTypes.add(arrayType.getIndexType(i));
            }
            if(instantiatedIndexTypes.size() != parameterizedIndexTypes.size()){
                isInstantiation = false;
            }else{
                for(int i = 0;i < instantiatedIndexTypes.size();++i ){
                    if(!isInstatiationOf(instantiatedIndexTypes.get(i),parameterizedIndexTypes.get(i))){
                        isInstantiation = false;
                        break;
                    }
                }
            }

            if(isInstantiation){

                instantiations.add(new ArrayInstantiationType(instantiatedIndexTypes,arrayAccessExpression.getType().getUnderlyingType(),arrayType));
            }
        }else{//does not contain type params
            addToInstantiationsIfApplicable(arrayAccessExpression.getType());
        }
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        //TODO unify body of this and array access expression into 1 procedure
        BoogieType overallType = arrayStoreExpression.getArray().getType().getUnderlyingType();
        if(isArrayTypeWithTypeParam(overallType)){
            if(!(overallType instanceof ArrayType) || !(overallType.getUnderlyingType() instanceof ArrayType)){
                //Not sure if this is possible at all, since we are in an arrayAccessExpression.
                throw new RuntimeException("Error: array type with type variable is contained within another type: "+overallType);
            }
            ArrayType arrayType = (ArrayType)overallType.getUnderlyingType();
            if(typeParamInstatiations.get(overallType) == null){
                typeParamInstatiations.put(arrayType,new HashSet<>());
            }
            Set<ArrayInstantiationType> instantiations = typeParamInstatiations.get(overallType);
            List<BoogieType> parameterizedIndexTypes = new LinkedList<>();

            boolean isInstantiation = true;
            List<BoogieType> instantiatedIndexTypes = Arrays.asList(arrayStoreExpression.getIndices()).stream().map(expression -> expression.getType().getUnderlyingType()).collect(Collectors.toList());
            for(int i = 0; i < arrayType.getIndexCount(); ++i){
                parameterizedIndexTypes.add(arrayType.getIndexType(i));
            }
            if(instantiatedIndexTypes.size() != parameterizedIndexTypes.size()){
                isInstantiation = false;
            }else{
                for(int i = 0;i < instantiatedIndexTypes.size();++i ){
                    if(!isInstatiationOf(instantiatedIndexTypes.get(i),parameterizedIndexTypes.get(i))){
                        isInstantiation = false;
                        break;
                    }
                }
            }

            if(isInstantiation){
                //instantiations.add(new ArrayInstantiationType(instantiatedIndexTypes,arrayStoreExpression.getType().getUnderlyingType(),arrayType));
                instantiations.add(new ArrayInstantiationType(instantiatedIndexTypes,arrayStoreExpression.getValue().getType(),arrayType));
            }
        }else{//does not contain type params
            addToInstantiationsIfApplicable(arrayStoreExpression.getType());
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        binaryExpression.getLeft().accept(this);
        binaryExpression.getRight().accept(this);
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        process(bitVectorAccessExpression.getChildren());
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        //process(codeExpression.getChildren());
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        addToInstantiationsIfApplicable(functionApplication.getType());
        process(functionApplication.getChildren());
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        addToInstantiationsIfApplicable(functionApplicationWithType.getType());
        process(functionApplicationWithType.getChildren());
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        addToInstantiationsIfApplicable(identifierExpression.getType());
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        process(ifThenElseExpression.getChildren());
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        process(quantifierExpression.getChildren());
    }


    //TODO move to top

    /**
     * add boogieType to the correct set of typeParamInstatiations, if it is applicable.
     * If boogie type is not an array type, nothing happens
     * @param boogieType
     * @return
     */
    private void addToInstantiationsIfApplicable(BoogieType boogieType){
        if(!(boogieType.getUnderlyingType() instanceof ArrayType)){
            return;
        }
        ArrayType arrayType = (ArrayType)(boogieType.getUnderlyingType());
        for(Map.Entry<ArrayType,Set<ArrayInstantiationType>> entry: typeParamInstatiations.entrySet()){
            ArrayType parameterziedType = entry.getKey();
            if(isInstatiationOf(arrayType,parameterziedType)){
                entry.getValue().add(new ArrayInstantiationType(arrayType,parameterziedType));
                //should we return here? No, theoretically it could be instantiation of 2 different parameterized types.
            }
        }
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        //TODO need to add this if it an instantiation of some other map.
        addToInstantiationsIfApplicable(lambdaExpression.getType());
        process(lambdaExpression.getChildren());
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        addToInstantiationsIfApplicable(unaryExpression.getType());
        process(unaryExpression.getChildren());
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        //nothing
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        process(ensuresSpecification.getChildren());
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        process(loopInvariantSpecification.getChildren());
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        process(modifiesSpecification.getChildren());
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        process(requiresSpecification.getChildren());
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        process(assertStatement.getChildren());
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        process(assignmentStatement.getChildren());
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        process(assumeStatement.getChildren());
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        //nothing
    }

    @Override
    public void visit(CallStatement callStatement) {
        process(callStatement.getChildren());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        //nothing
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        process(havocStatement.getChildren());
    }

    @Override
    public void visit(IfStatement ifStatement) {
        process(ifStatement.getChildren());
    }

    @Override
    public void visit(Label label) {
        //nothing
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        process(parallelCall.getChildren());
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        //nothing
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        process(whileStatement.getChildren());
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        //nothing
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        BoogieType overallType = arrayLHS.getArray().getType().getUnderlyingType();
        if(isArrayTypeWithTypeParam(overallType)){
            if(!(overallType instanceof ArrayType) || !(overallType.getUnderlyingType() instanceof ArrayType)){
                //Not sure if this is possible at all, since we are in an arrayAccessExpression.
                throw new RuntimeException("Error: array type with type variable is contained within another type: "+overallType);
            }
            ArrayType arrayType = (ArrayType)overallType.getUnderlyingType();
            if(typeParamInstatiations.get(overallType) == null){
                typeParamInstatiations.put(arrayType,new HashSet<>());
            }
            Set<ArrayInstantiationType> instantiations = typeParamInstatiations.get(overallType);
            List<BoogieType> parameterizedIndexTypes = new LinkedList<>();

            boolean isInstantiation = true;
            List<BoogieType> instantiatedIndexTypes = Arrays.asList(arrayLHS.getIndices()).stream().map(expression -> expression.getType().getUnderlyingType()).collect(Collectors.toList());
            for(int i = 0; i < arrayType.getIndexCount(); ++i){
                parameterizedIndexTypes.add(arrayType.getIndexType(i));
            }
            if(instantiatedIndexTypes.size() != parameterizedIndexTypes.size()){
                isInstantiation = false;
            }else{
                for(int i = 0;i < instantiatedIndexTypes.size();++i ){
                    if(!isInstatiationOf(instantiatedIndexTypes.get(i),parameterizedIndexTypes.get(i))){
                        isInstantiation = false;
                        break;
                    }
                }
            }

            if(isInstantiation){

                instantiations.add(new ArrayInstantiationType(instantiatedIndexTypes,arrayLHS.getType().getUnderlyingType(),arrayType));
            }
        }else{//does not contain type params
            addToInstantiationsIfApplicable(arrayLHS.getType());
        }


        //Log.error("might have missed instantiations");
        //throw new RuntimeException("type param of arrayLHS needs to be determined");
    }

    @Override
    public void visit(Body body) {
        process(body.getChildren());
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {
        //nothing
    }

    @Override
    public void visit(ParentEdge parentEdge) {
        //nothing
    }

    @Override
    public void visit(Project project) {
        //nothing
    }

    @Override
    public void visit(Trigger trigger) {
        //nothing
    }

    @Override
    public void visit(Unit unit) {
        process(unit.getChildren());
    }

    @Override
    public void visit(VariableLHS variableLHS) {
        addToInstantiationsIfApplicable(variableLHS.getType());
        process(variableLHS.getChildren());
    }

    @Override
    public void visit(VarList varList) {
        if(varList.getWhereClause() != null){
            varList.getWhereClause().accept(this);
        }
        //TODO visit type?
        addToInstantiationsIfApplicable(varList.getType().getBoogieType().getUnderlyingType());
        process(varList.getChildren());
    }

    @Override
    public void visit(Axiom axiom) {
        process(axiom.getChildren());
    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {
        process(constDeclaration.getChildren());
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        //TODO change to process
        for(VarList varList: functionDeclaration.getInParams()){
            varList.accept(this);
        }
        if(functionDeclaration.getOutParam() != null){
            functionDeclaration.getOutParam().accept(this);
        }
        if(functionDeclaration.getBody() != null){
            functionDeclaration.getBody().accept(this);
        }

    }

    @Override
    public void visit(Implementation implementation) {
        process(implementation.getChildren());
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        process(procedureDeclaration.getChildren());
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        //TODO test
        if(typeDeclaration.getSynonym() == null){
            return;
        }
        if(typeDeclaration.getSynonym().getBoogieType() instanceof ArrayType){
            ArrayType arrayType = (ArrayType)typeDeclaration.getSynonym().getBoogieType().getUnderlyingType();
            if(isArrayTypeWithTypeParam(arrayType)){
                if(typeParamInstatiations.get(arrayType) == null){
                    typeParamInstatiations.put(arrayType,new HashSet<>());
                }
            }
        }
        addToInstantiationsIfApplicable(typeDeclaration.getSynonym().getBoogieType());
    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {




        process(variableDeclaration.getChildren());
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {
        //TODO might need the information of what the type parameters are actually named? that is lost in BoogieType (that uses de brujin style)
        addToInstantiationsIfApplicable(arrayAstType.getBoogieType());
        ArrayType arrayType = (ArrayType)arrayAstType.getBoogieType().getUnderlyingType();
        if(arrayAstType.getTypeParams().length > 0){
            typeParamInstatiations.put(arrayType,new HashSet<>());
        }
        //throw new RuntimeException("array ast type type param finder");
    }

    @Override
    public void visit(NamedAstType namedAstType) {
        addToInstantiationsIfApplicable(namedAstType.getBoogieType().getUnderlyingType());
    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        //nothing
    }
}
