package boogie2WhyML.Helper.PolymorphicMap;

import boogie.ast.*;
import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.location.ILocation;
import boogie.ast.specification.*;
import boogie.ast.statement.*;
import boogie.type.*;
import boogie2WhyML.BoogieTranslator;
import boogie2WhyML.ErrorMessages;
import javafx.util.Pair;
import typechecker.TypeChecker;
import util.Log;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Replace global variables of the form var h: <a>[a*]a*, with multiple variables which represent concrete instantiations
 * of this variable
 * e.g.
 *
 * type Heap = <a>[ref, Field a]a;
 *
 * var heap: Heap;
 * ==>
 *
 * var heap_0: [ref, Field bool]bool;
 * var heap_1: [ref, Field int]int;
 */
public class GlobalVariableReplacer implements ASTVisitor{

    Map<ArrayType, Set<ArrayInstantiationType>> typeParamInstantiations;

    //e.g. heap ==> [heap_0,heap_1];
    Map<String, List<String>> globalVariableReplacements;
    Map<String, ASTType> newVariableConcreteType;
    Map<Pair<String,BoogieType>,String> concreteVarNameGlobal;

    String newVarPrefix = "v_";
    String newConstPrefix = "c_";

    /**
     * generates new delcerations, based on splitting variables which are of map type with type parameter into their instantiations.
     * //TODO IMPORTANT: for now, requires shadowed variables to have already been renamed (specifically, if a variable of polymorhpic map type is shadowed, it might cause problems.
     *
     * @param decls
     * @param typeParamFinder
     */
    public GlobalVariableReplacer(Declaration[] decls, TypeParamFinder typeParamFinder){
        this.typeParamInstantiations = typeParamFinder.getTypeParamInstatiations();
        this.globalVariableReplacements = new HashMap<>();
        this.newDeclarations = new LinkedList<>();
        this.newVariableConcreteType = new HashMap<>();
        this.concreteVarNameGlobal = new HashMap<>();
        this.resultNode = null;
        //
        for(Declaration d: decls){
            if(d instanceof VariableDeclaration) {
                VariableDeclaration vd = (VariableDeclaration) d;
                List<VarList> newVarLists = new LinkedList<>();
                for (VarList varList : vd.getVariables()) {
                    Set<ArrayInstantiationType> arrayInstantiationTypes = typeParamInstantiations.get(varList.getType().getBoogieType().getUnderlyingType());
                    if (arrayInstantiationTypes != null) {//TODO what if size is 0??? -> should use dummy type, so size should never be zero.
                        if (arrayInstantiationTypes.size() == 0) {
                            //throw new RuntimeException("No concrete instantiations -> need to use 'dummy' type");
                            Log.error("NOT using dummy type for now. omitting variable, since it has no concrete instatiation.");
                            continue;
                        }
                        //this VarList needs to be replaced by concrete instantiations.
                        for (String ident : varList.getIdentifiers()) {
                            int counter = 0;
                            if (globalVariableReplacements.get(ident) != null) {
                                throw new RuntimeException("Error: globalVariableReplacement of " + ident + " should be null");
                            }
                            globalVariableReplacements.put(ident, new LinkedList<>());
                            for (ArrayInstantiationType ait : arrayInstantiationTypes) {
                                String newIdent = newVarPrefix + ident + (counter++);
                                globalVariableReplacements.get(ident).add(newIdent);
                                ASTType newType = concreteBoogieTypeToASTType(ait.getConcreteType());
                                if (newVariableConcreteType.get(newIdent) != null) {
                                    throw new RuntimeException("Error: new variable already created... need to rename");
                                }
                                if (concreteVarNameGlobal.get(new Pair<>(ident, ait.getConcreteType())) != null) {
                                    throw new RuntimeException("Error: already have a name for instantiation of this variable");
                                }
                                newVariableConcreteType.put(newIdent, newType);
                                concreteVarNameGlobal.put(new Pair<>(ident, ait.getConcreteType()), newIdent);
                                //TODO is it ok to split where clause like this?
                                newVarLists.add(new VarList(null, new String[]{newIdent}, newType, varList.getWhereClause()));
                            }
                        }
                    } else {
                        newVarLists.add(varList);
                    }
                }
                newDeclarations.add(new VariableDeclaration(d.getLocation(), d.getAttributes(), newVarLists.toArray(new VarList[newVarLists.size()])));
            }else if(d instanceof ConstDeclaration){
                Log.error("ConstDeclaration might need to be replaced by instantiations.");
                newDeclarations.add(d);
            }
        }
        //TODO fill out visitor to modify access to variables of these types.
        for(Declaration d: decls){
            if(! (d instanceof VariableDeclaration)){
                d.accept(this);
            }
        }
    }

    /**
     * creates a new unit with the new declarations, type checks the ast and returns both the unit and typechecker.
     * @return
     */
    public Pair<Unit,TypeChecker> getNewTypeCheckedUnit(){
        Unit u = new Unit(null,newDeclarations.toArray(new Declaration[newDeclarations.size()]));
        TypeChecker typeChecker = new TypeChecker(u);
        return new Pair<>(u,typeChecker);
    }


    private List<Declaration> newDeclarations;

    public List<Declaration> getNewDeclarations() {
        return newDeclarations;
    }

    public Declaration[] getNewDeclarationsArray(){
        return newDeclarations.toArray(new Declaration[newDeclarations.size()]);
    }

    public Map<String, ASTType> getNewVariableConcreteType() {
        return newVariableConcreteType;
    }

    /**
     *
     * @return a map which:
     * given a pair of: original variable name, wanted instantiation type, the name of the declared variable of that type
     */
    public Map<Pair<String, BoogieType>, String> getConcreteVarNameGlobal() {
        return concreteVarNameGlobal;
    }

    /**
     * return an equivalent ASTType
     * ONLY concrete types are allowed. I.e. no Placeholders.
     * @param boogieType
     * @return
     */
    public ASTType concreteBoogieTypeToASTType(BoogieType boogieType){
        if(boogieType instanceof PlaceholderType){
            throw new RuntimeException("Error: only concrete types are allowed.");
        }else if(boogieType instanceof PrimitiveType){
            return new PrimitiveAstType(null,boogieType,((PrimitiveType)boogieType).toString(0,false));
        }else if(boogieType instanceof ConstructedType){
            ConstructedType constructedType = (ConstructedType)boogieType;
            String name = constructedType.getConstr().getName();
            int argCount = constructedType.getConstr().getParamCount();
            ASTType[] typeArguments = new ASTType[argCount];
            for(int i = 0; i < argCount; ++i){
                typeArguments[i] = concreteBoogieTypeToASTType(constructedType.getParameter(i));
            }
            return new NamedAstType(null,boogieType,name,typeArguments);
        }else if(boogieType instanceof ArrayType){
            ArrayType arrayType = (ArrayType)boogieType;
            //we assume there are no type params.
            String[] typeParams = new String[0];
            ASTType valueType = concreteBoogieTypeToASTType(arrayType.getValueType());
            int indexCount = arrayType.getIndexCount();
            ASTType[] indexTypes = new ASTType[indexCount];
            for(int i = 0; i < indexCount; ++i){
                indexTypes[i] = concreteBoogieTypeToASTType(arrayType.getIndexType(i));
            }
            return new ArrayAstType(null,boogieType,typeParams,indexTypes,valueType);
        }else{
            throw new RuntimeException("Error: unknown Boogie type");
        }
    }


    private ASTNode resultNode;

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        resultNode = bitvecLiteral.clone();
    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        resultNode = booleanLiteral.clone();
    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {
        resultNode = integerLiteral.clone();
    }

    @Override
    public void visit(RealLiteral realLiteral) {
        resultNode = realLiteral.clone();
    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        resultNode = stringLiteral.clone();
    }

    private ArrayType concreteType = null;

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        BoogieType[] indices = Arrays.asList(arrayAccessExpression.getIndices()).
                stream().map(expression -> expression.getType()).collect(Collectors.toList()).
                toArray(new BoogieType[arrayAccessExpression.getIndices().length]);
        BoogieType valueType = arrayAccessExpression.getType();
        //TODO placeholder count... possible that it's not 0, if it is not a concrete type.
        ArrayType type = BoogieType.createArrayType(0,indices,valueType);
        if(TypeParamFinder.containsTypeParams(type)){
            //throw new RuntimeException("implement splitting array accessExpression based on concrete types.");
            Log.error("WARNING: implement splitting array accessExpression based on concrete types.");
            String warningMessage = ErrorMessages.ARRAY_ACCESS_EXPRESSION.getWarning();
            BoogieTranslator.issueWarning(arrayAccessExpression.getLocation(),warningMessage);
            resultNode = arrayAccessExpression.clone();
            return;
            //TODO need to create multiple arrayAccessExpressions, probably also split higher up.
        }
        ArrayType concreteArrayType = ArrayInstantiationType.getConcreteType(arrayAccessExpression);
        concreteType = concreteArrayType;
        arrayAccessExpression.getArray().accept(this);
        concreteType = null;
        Expression newArray = (Expression)resultNode;
        Expression[] oldIndices = arrayAccessExpression.getIndices();
        Expression[] newIndices = new Expression[oldIndices.length];
        for(int i = 0; i < oldIndices.length; ++i){
            oldIndices[i].accept(this);
            newIndices[i] = (Expression)resultNode;
        }
        resultNode = new ArrayAccessExpression(arrayAccessExpression.getLocation(),arrayAccessExpression.getType(),newArray,newIndices);
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        BoogieType[] indices = Arrays.asList(arrayStoreExpression.getIndices()).
                stream().map(expression -> expression.getType()).collect(Collectors.toList()).
                toArray(new BoogieType[arrayStoreExpression.getIndices().length]);
        BoogieType valueType = arrayStoreExpression.getValue().getType();
        ArrayType type = ArrayInstantiationType.getArrayType(indices, valueType);
        if(TypeParamFinder.containsTypeParams(type)){
            //throw new RuntimeException("implement splitting array accessExpression based on concrete types.");
            Log.error("WARNING: implement splitting array store expression based on concrete types.");
            String warningMessage = ErrorMessages.ARRAY_STORE_EXPRESSION.getWarning();
            BoogieTranslator.issueWarning(arrayStoreExpression.getLocation(),warningMessage);
            resultNode = arrayStoreExpression.clone();
            return;
            //TODO need to create multiple arrayAccessExpressions, probably also split higher up.
        }
        concreteType = type;
        arrayStoreExpression.getArray().accept(this);
        concreteType = null;
        Expression newArray = (Expression)resultNode;
        Expression[] oldIndices = arrayStoreExpression.getIndices();
        Expression[] newIndices = new Expression[oldIndices.length];
        for(int i = 0; i < oldIndices.length; ++i){
            oldIndices[i].accept(this);
            newIndices[i] = (Expression)resultNode;
        }
        arrayStoreExpression.getValue().accept(this);
        Expression newValue = (Expression)resultNode;
        resultNode = new ArrayStoreExpression(arrayStoreExpression.getLocation(),arrayStoreExpression.getType(),newArray,newIndices,newValue);
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        binaryExpression.getLeft().accept(this);
        Expression newLeft = (Expression)resultNode;
        binaryExpression.getRight().accept(this);
        Expression newRight = (Expression)resultNode;
        //TODO new type and operator correct? or could it change with concrete instantiation?
        resultNode = new BinaryExpression(binaryExpression.getLocation(),binaryExpression.getType(),binaryExpression.getOperator(),newLeft,newRight);
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        bitVectorAccessExpression.getBitvec().accept(this);
        resultNode = new BitVectorAccessExpression(bitVectorAccessExpression.getLocation(),
                bitVectorAccessExpression.getType(),(Expression)resultNode, bitVectorAccessExpression.getEnd(),
                bitVectorAccessExpression.getStart());
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        resultNode = codeExpression.clone();
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        //todo: potentially, 1 functionapplication could turn into multiple, if function declaration has type params.
        Log.error("variable replacement for function application needs work.");
        Expression[] oldArgs = functionApplication.getArguments();
        Expression[] newArgs = new Expression[oldArgs.length];
        for(int i = 0; i < oldArgs.length; ++i){
            oldArgs[i].accept(this);
            newArgs[i] = (Expression)resultNode;
        }
        resultNode = new FunctionApplication(functionApplication.getLocation(),functionApplication.getType(),
                functionApplication.getIdentifier(),newArgs);
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        //todo: this has a declared return type, so perhaps use new function declaration?
        Log.error("variable replacement for function application with type needs work.");
        Expression[] oldArgs = functionApplicationWithType.getArguments();
        Expression[] newArgs = new Expression[oldArgs.length];
        for(int i = 0; i < oldArgs.length; ++i){
            oldArgs[i].accept(this);
            newArgs[i] = (Expression)resultNode;
        }
        resultNode = new FunctionApplication(functionApplicationWithType.getLocation(),functionApplicationWithType.getType(),
                functionApplicationWithType.getIdentifier(),newArgs);
    }

    /**
     *  //TODO add check for local and global variables
     * @param name
     * @return true, iff name is the name of a global or local variable or constant, with a parameterized map type.
     */
    private boolean needsToBeReplaced(String name){
        return globalVariableReplacements.get(name) != null;
    }

    /**
     * TODO add local variables.
     * return a list of variable names which represent concrete types of old variables.
     * e.g. var x: Heap;
     * ==>
     * [x_0,x_1,...,x_n]
     * @param oldIdent
     * @return null, if this variable is not going to be replaced.
     */
    private List<String> getReplacements(String oldIdent){
        return globalVariableReplacements.get(oldIdent);
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        String name = identifierExpression.getIdentifier();
        String newVarName = name;
        BoogieType newType = identifierExpression.getType();
        if(needsToBeReplaced(name)){
            newVarName = concreteVarNameGlobal.get(new Pair<>(name,concreteType));
            if(newVarName == null){
                throw new RuntimeException("Error: identifier expression should be replaced, but couldn't. Not a concrete type?");
            }
            newType = concreteType;
        }
        resultNode = new IdentifierExpression(identifierExpression.getLocation(),newType,newVarName);
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        ifThenElseExpression.getCondition().accept(this);
        Expression newCondition = (Expression)resultNode;
        ifThenElseExpression.getThenPart().accept(this);
        Expression newThenPart = (Expression)resultNode;
        ifThenElseExpression.getElsePart().accept(this);
        Expression newElsePart = (Expression)resultNode;
        resultNode = new IfThenElseExpression(ifThenElseExpression.getLocation(),newCondition,newThenPart,newElsePart);
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        Attribute[] oldAttributes = quantifierExpression.getAttributes();
        Attribute[] newAttributes = new Attribute[oldAttributes.length];
        for(int i = 0; i < oldAttributes.length; ++i){
            oldAttributes[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        boolean isUniversal = quantifierExpression.isUniversal();
        String[] newTypeParams = Arrays.copyOf(quantifierExpression.getTypeParams(),quantifierExpression.getTypeParams().length);
        //TODO how to handle parameters? do they need bo be visited? generate multiple parameters for those which are polymorphic type?
        VarList[] oldParameters = quantifierExpression.getParameters();
        VarList[] newParameters = new VarList[oldParameters.length];
        for(int i = 0; i < oldParameters.length; ++i){
            oldParameters[i].accept(this);
            newParameters[i] = (VarList)resultNode;
        }
        quantifierExpression.getSubformula().accept(this);
        Expression newSubformula = (Expression)resultNode;
        ILocation newLocation = quantifierExpression.getLocation();
        //TODO copy type or perform type checking again?
        BoogieType newType = quantifierExpression.getType();
        resultNode = new QuantifierExpression(newLocation,newType,isUniversal,newTypeParams,newParameters,newAttributes,newSubformula);
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        if(lambdaExpression.getTypeParams().length > 0){
            BoogieTranslator.issueWarning(lambdaExpression.getLocation(),ErrorMessages.LAMBDA_EXPRESSION.getWarning());
        }
        //how to handle parameters
        //throw new RuntimeException("replace vars in lambda expression");
        resultNode = lambdaExpression;
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        unaryExpression.getExpr().accept(this);
        resultNode = new UnaryExpression(unaryExpression.getLocation(),unaryExpression.getType(),unaryExpression.getOperator(),(Expression)resultNode);
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        resultNode = new WildcardExpression(wildcardExpression.getLocation(),wildcardExpression.getType());
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        ensuresSpecification.getFormula().accept(this);
        resultNode = new EnsuresSpecification(ensuresSpecification.getLocation(),ensuresSpecification.isFree(),(Expression)resultNode);
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        loopInvariantSpecification.getFormula().accept(this);
        resultNode = new LoopInvariantSpecification(loopInvariantSpecification.getLocation(),loopInvariantSpecification.isFree(),(Expression)resultNode);
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        List<String> newModifies = new LinkedList<>();
        for(String oldIdent: modifiesSpecification.getIdentifiers()){
            if(needsToBeReplaced(oldIdent)){
                newModifies.addAll(getReplacements(oldIdent));
            }else{
                newModifies.add(oldIdent);
            }
        }
        resultNode = new ModifiesSpecification(modifiesSpecification.getLocation(),modifiesSpecification.isFree(),newModifies.toArray(new String[newModifies.size()]));
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        requiresSpecification.getFormula().accept(this);
        resultNode = new RequiresSpecification(requiresSpecification.getLocation(),requiresSpecification.isFree(),(Expression)resultNode);
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        Attribute[] newAttributes = new Attribute[assertStatement.getAttributes().length];
        for(int i = 0; i < assertStatement.getAttributes().length; ++i){
            assertStatement.getAttributes()[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        assertStatement.getFormula().accept(this);
        Expression newFormula = (Expression)resultNode;
        resultNode = new AssertStatement(assertStatement.getLocation(),newAttributes,newFormula);
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

        LeftHandSide[] oldLHS = assignmentStatement.getLhs();
        List<LeftHandSide> newLHS = new LinkedList<>();
        Expression[] oldRHS = assignmentStatement.getRhs();
        List<Expression> newRHS = new LinkedList<>();
        if(oldLHS.length != oldRHS.length){
            throw new RuntimeException("Error: assignment doesn't have same amount of expressions on left and right side: "+assignmentStatement);
        }
        for(int i = 0; i < oldLHS.length; ++i){
            LeftHandSide lhs = oldLHS[i];
            Expression rhs = oldRHS[i];
            if((lhs.getType().getUnderlyingType() instanceof ArrayType) && (((ArrayType)lhs.getType().getUnderlyingType()).getNumPlaceholders() > 0)){
            //if(TypeParamFinder.containsTypeParams(lhs.getType())){
                BoogieType actualRightType = ArrayInstantiationType.actualType(rhs);
                if(!(actualRightType instanceof ArrayType)){
                    throw new RuntimeException("Error: split LHS based on concrete type: "+lhs);
                }
                if(TypeParamFinder.containsTypeParams(actualRightType)){
                    throw new RuntimeException("Error: split LHS based on concrete type: "+lhs);
                }
                concreteType = (ArrayType)actualRightType;
            }
            lhs.accept(this);
            newLHS.add((LeftHandSide) resultNode);
            concreteType = null;
            rhs.accept(this);
            newRHS.add((Expression)resultNode);
        }
        LeftHandSide[] newLHSArr = newLHS.toArray(new LeftHandSide[newLHS.size()]);
        Expression[] newRHSarr = newRHS.toArray(new Expression[newRHS.size()]);
        resultNode = new AssignmentStatement(assignmentStatement.getLocation(),newLHSArr,newRHSarr);

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        assumeStatement.getFormula().accept(this);
        resultNode = new AssumeStatement(assumeStatement.getLocation(),assumeStatement.getAttributes(),(Expression)resultNode);
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        resultNode = new BreakStatement(breakStatement.getLocation(),breakStatement.getLabel());
    }

    @Override
    public void visit(CallStatement callStatement) {
        //we assume a procedure returns mulitple values instead of being splitted. Otherwise this needs to return multiple call statements.
        if(callStatement.isForall()){
            resultNode = callStatement;
        }
        Expression[] oldArgs = callStatement.getArguments();
        Expression[] newArgs = new Expression[oldArgs.length];
        for(int i = 0; i < oldArgs.length; ++i){
            oldArgs[i].accept(this);
            newArgs[i] = (Expression)resultNode;
        }

        String[] oldLHS = callStatement.getLhs();
        List<String> newLHS = new LinkedList<>();
        for(String lhs: oldLHS){
            if(needsToBeReplaced(lhs)){
                newLHS.addAll(getReplacements(lhs));
            }else{
                newLHS.add(lhs);
            }
        }
        //TODO attributes are lost...
        resultNode = new CallStatement(callStatement.getLocation(),callStatement.isForall(),
                newLHS.toArray(new String[newLHS.size()]),callStatement.getMethodName(),callStatement.getArguments());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        resultNode = new GotoStatement(gotoStatement.getLocation(),gotoStatement.getLabels());
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        List<String> newIdents = new LinkedList<>();
        for(String ident:havocStatement.getIdentifiers()){
            if(needsToBeReplaced(ident)){
                newIdents.addAll(getReplacements(ident));
            }else{
                newIdents.add(ident);
            }
        }
        String[] newIdentsArray = newIdents.toArray(new String[newIdents.size()]);
        resultNode = new HavocStatement(havocStatement.getLocation(),havocStatement.getAttributes(),newIdentsArray);
    }

    @Override
    public void visit(IfStatement ifStatement) {
        ifStatement.getCondition().accept(this);
        Expression newCondition = (Expression)resultNode;
        Statement[] newThenPart = new Statement[ifStatement.getThenPart().length];
        for(int i = 0; i < newThenPart.length; ++i){
            ifStatement.getThenPart()[i].accept(this);
            newThenPart[i] = (Statement)resultNode;
        }
        Statement[] newElsePart = new Statement[ifStatement.getElsePart().length];
        for(int i = 0; i < newElsePart.length; ++i){
            ifStatement.getElsePart()[i].accept(this);
            newElsePart[i] = (Statement)resultNode;
        }
        resultNode = new IfStatement(ifStatement.getLocation(),newCondition,newThenPart,newElsePart);
    }

    @Override
    public void visit(Label label) {
        resultNode = new Label(label.getLocation(),label.getName());
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        throw new RuntimeException("Error: parallel call not supported.");
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        resultNode = new ReturnStatement(returnStatement.getLocation(),returnStatement.getExpression());
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        whileStatement.getCondition().accept(this);
        Expression condition = (Expression)resultNode;
        Statement[] newBody = new Statement[whileStatement.getBody().length];
        for(int i = 0; i < newBody.length; ++i){
            whileStatement.getBody()[i].accept(this);
            newBody[i] = (Statement)resultNode;
        }
        LoopInvariantSpecification[] invariants = new LoopInvariantSpecification[whileStatement.getInvariants().length];
        for(int i = 0; i < invariants.length; ++i){
            whileStatement.getInvariants()[i].accept(this);
            invariants[i] = (LoopInvariantSpecification)resultNode;
        }
        resultNode = new WhileStatement(whileStatement.getLocation(),condition,invariants,newBody);
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        resultNode = new YieldStatement(yieldStatement.getLocation());
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        BoogieType[] indices = Arrays.asList(arrayLHS.getIndices()).
                stream().map(expression -> expression.getType()).collect(Collectors.toList()).
                toArray(new BoogieType[arrayLHS.getIndices().length]);
        BoogieType valueType = arrayLHS.getType();
        //TODO placeholder count... possible that it's not 0, if it is not a concrete type.
        //actually, placeholder MUST be 0 in our model right?
        ArrayType type = BoogieType.createArrayType(0,indices,valueType);
        if(TypeParamFinder.containsTypeParams(type)){
            //throw new RuntimeException("implement splitting array accessExpression based on concrete types.");
            Log.error("implement splitting array arrayLHS based on concrete types.");
            resultNode = arrayLHS;
            return;
            //TODO need to create multiple arrayAccessExpressions, probably also split higher up.
        }
        ArrayType concreteArrayType = type;
        concreteType = concreteArrayType;
        arrayLHS.getArray().accept(this);
        concreteType = null;
        LeftHandSide newArray = (LeftHandSide)resultNode;
        Expression[] oldIndices = arrayLHS.getIndices();
        Expression[] newIndices = new Expression[oldIndices.length];
        for(int i = 0; i < oldIndices.length; ++i){
            oldIndices[i].accept(this);
            newIndices[i] = (Expression)resultNode;
        }
        resultNode = new ArrayLHS(arrayLHS.getLocation(),arrayLHS.getType(),newArray,newIndices);
    }

    @Override
    public void visit(Body body) {
        VariableDeclaration[] oldVariableDecls = body.getLocalVars();
        //need to split local variable declarations if they are polymorphic map types. (Just as with global ones)
        VariableDeclaration[] newVariableDeclarations = Arrays.copyOf(oldVariableDecls, oldVariableDecls.length);
        Statement[] newStatements = new Statement[body.getBlock().length];
        for(int i = 0; i < body.getBlock().length; ++i){
            body.getBlock()[i].accept(this);
            newStatements[i] = (Statement)resultNode;
        }
        resultNode = new Body(body.getLocation(),newVariableDeclarations,newStatements);
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {
        //we do NOT change the name. expressions might change
        Expression[] values = new Expression[namedAttribute.getValues().length];
        for(int i = 0; i < values.length;++i){
            namedAttribute.getValues()[i].accept(this);
            values[i] = (Expression)resultNode;
        }
        resultNode = new NamedAttribute(namedAttribute.getLocation(),namedAttribute.getName(),values);
    }

    @Override
    public void visit(ParentEdge parentEdge) {

        String newName = parentEdge.getIdentifier();
        if(needsToBeReplaced(newName)){
            throw new RuntimeException("Error: parent edge needs to be replaced by potentially many edges. Should have benn " +
                    "replaced already.");
        }
        resultNode = new ParentEdge(parentEdge.getLocation(),parentEdge.isUnique(),newName);

    }

    @Override
    public void visit(Project project) {
        Log.error("project not visited.");
    }

    @Override
    public void visit(Trigger trigger) {
        Expression[] newTriggers = new Expression[trigger.getTriggers().length];
        for(int i = 0; i < newTriggers.length; ++i){
            trigger.getTriggers()[i].accept(this);
            newTriggers[i] = (Expression)resultNode;
        }
        resultNode = new Trigger(trigger.getLocation(),newTriggers);
    }

    @Override
    public void visit(Unit unit) {
        Log.error("Unit not visited: "+unit);
    }

    @Override
    public void visit(VariableLHS variableLHS) {
        String name = variableLHS.getIdentifier();
        String newVarName = name;
        BoogieType newType = variableLHS.getType();
        if(needsToBeReplaced(name)){
            //TODO add for local variables.
            newVarName = concreteVarNameGlobal.get(new Pair<>(name, concreteType));
            if(newVarName == null){
                throw new RuntimeException("Error: identifier expression should be replaced, but couldn't. Not a concrete type ?");
            }
            newType = concreteType;
        }
        resultNode = new VariableLHS(variableLHS.getLocation(),newType,newVarName);
    }

    @Override
    public void visit(VarList varList) {
        //todo for now, just return varlist
        Log.error("varlist not turned into concrete types: "+varList);
        resultNode = varList;
    }

    @Override
    public void visit(Axiom axiom) {
        Attribute[] newAttributes = new Attribute[axiom.getAttributes().length];
        for(int i = 0; i < newAttributes.length; ++i){
            axiom.getAttributes()[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        axiom.getFormula().accept(this);
        Expression newFormula = (Expression)resultNode;
        newDeclarations.add(new Axiom(axiom.getLocation(),newAttributes,newFormula));
    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {
        //nothing should be done here, but in beginning it should be treated same as variable declaration.
    }



    @Override
    public void visit(Implementation implementation) {
        //TODO for in and out parameters, might need to split procedure if it is polymorphic.
        Attribute[] newAttributes = new Attribute[implementation.getAttributes().length];
        for(int i = 0; i < implementation.getAttributes().length; ++i){
            implementation.getAttributes()[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        VarList[] inParamsNew = new VarList[implementation.getInParams().length];
        for(int i = 0; i < implementation.getInParams().length; ++i){
            implementation.getInParams()[i].accept(this);
            inParamsNew[i] = (VarList)resultNode;
        }
        VarList[] outParamsNew = new VarList[implementation.getOutParams().length];
        for(int i = 0; i < implementation.getOutParams().length; ++i){
            implementation.getOutParams()[i].accept(this);
            outParamsNew[i] = (VarList)resultNode;
        }
        Specification[] newSpecs = new Specification[implementation.getSpecification().length];
        for(int i = 0; i < implementation.getSpecification().length; ++i){
            implementation.getSpecification()[i].accept(this);
            newSpecs[i] = (Specification)resultNode;
        }

        Body newBody = null;
        if(implementation.getBody() != null){
            implementation.getBody().accept(this);
            newBody = (Body)resultNode;
        }



        //strings are immutable
        String newIdentifier = implementation.getIdentifier();
        String[] newTypeParams = Arrays.copyOf(implementation.getTypeParams(),implementation.getTypeParams().length);
        if(newTypeParams.length > 0){
            Log.error("procedure declaration with type params might need to be split.");
        }

        newDeclarations.add(new Implementation(implementation.getLocation(),newAttributes,newIdentifier,newTypeParams,inParamsNew,outParamsNew,
                newSpecs,newBody));
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        //TODO for in and out parameters, might need to split procedure if it is polymorphic.
        Attribute[] newAttributes = new Attribute[procedureDeclaration.getAttributes().length];
        for(int i = 0; i < procedureDeclaration.getAttributes().length; ++i){
            procedureDeclaration.getAttributes()[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        VarList[] inParamsNew = new VarList[procedureDeclaration.getInParams().length];
        for(int i = 0; i < procedureDeclaration.getInParams().length; ++i){
            procedureDeclaration.getInParams()[i].accept(this);
            inParamsNew[i] = (VarList)resultNode;
        }
        VarList[] outParamsNew = new VarList[procedureDeclaration.getOutParams().length];
        for(int i = 0; i < procedureDeclaration.getOutParams().length; ++i){
            procedureDeclaration.getOutParams()[i].accept(this);
            outParamsNew[i] = (VarList)resultNode;
        }
        Specification[] newSpecs = new Specification[procedureDeclaration.getSpecification().length];
        for(int i = 0; i < procedureDeclaration.getSpecification().length; ++i){
            procedureDeclaration.getSpecification()[i].accept(this);
            newSpecs[i] = (Specification)resultNode;
        }

        Body newBody = null;
        if(procedureDeclaration.getBody() != null){
            procedureDeclaration.getBody().accept(this);
            newBody = (Body)resultNode;
        }



        //strings are immutable
        String newIdentifier = procedureDeclaration.getIdentifier();
        String[] newTypeParams = Arrays.copyOf(procedureDeclaration.getTypeParams(),procedureDeclaration.getTypeParams().length);
        if(newTypeParams.length > 0){
            Log.error("procedure declaration with type params might need to be split.");
        }

        newDeclarations.add(new ProcedureDeclaration(procedureDeclaration.getLocation(),newAttributes,newIdentifier,newTypeParams,inParamsNew,outParamsNew,
                newSpecs,newBody));
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        //TODO split if it has type parameters.
        Attribute[] newAttributes = new Attribute[functionDeclaration.getAttributes().length];
        for(int i = 0; i < functionDeclaration.getAttributes().length; ++i){
            functionDeclaration.getAttributes()[i].accept(this);
            newAttributes[i] = (Attribute)resultNode;
        }
        VarList[] inParamsNew = new VarList[functionDeclaration.getInParams().length];
        for(int i = 0; i < functionDeclaration.getInParams().length; ++i){
            functionDeclaration.getInParams()[i].accept(this);
            inParamsNew[i] = (VarList)resultNode;
        }
        functionDeclaration.getOutParam().accept(this);
        VarList outParamNew = (VarList)resultNode;

        Expression newBody = null;
        if(functionDeclaration.getBody() != null){
            functionDeclaration.getBody().accept(this);
            newBody = (Expression)resultNode;
        }

        //strings are immutable
        String newIdentifier = functionDeclaration.getIdentifier();
        String[] newTypeParams = Arrays.copyOf(functionDeclaration.getTypeParams(),functionDeclaration.getTypeParams().length);
        if(newTypeParams.length > 0){
            Log.error("function declaration with type params might need to be split.");
        }
        newDeclarations.add(new FunctionDeclaration(functionDeclaration.getLocation(),newAttributes,newIdentifier,
                newTypeParams,inParamsNew,outParamNew,newBody));
        //throw new RuntimeException("Global variable replacer for function declaration.");
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        newDeclarations.add(typeDeclaration);
    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        //this is done in the beginning, no need to do anything now.
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {
        throw new RuntimeException("Error: arrayASTType should not be visited by global variable replacer");
    }

    @Override
    public void visit(NamedAstType namedAstType) {
        throw new RuntimeException("Error: namedAstType should not be visited by global variable replacer");
    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        resultNode = primitiveAstType;
    }
}
