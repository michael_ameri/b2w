package boogie2WhyML.Helper.PolymorphicMap;


import boogie.ast.expression.ArrayAccessExpression;
import boogie.ast.expression.ArrayStoreExpression;
import boogie.ast.expression.Expression;
import boogie.type.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayInstantiationType {
    private List<BoogieType> indexTypes;
    private BoogieType valueType;

    private ArrayType parameterizedType;

    /**
     * we assume concreteArrayType has no placeholders and no type parameters.
     * @param concreteArrayType
     */
    public ArrayInstantiationType(ArrayType concreteArrayType, ArrayType parameterizedType){
        indexTypes = new LinkedList<>();
        for(int i = 0; i < concreteArrayType.getIndexCount(); ++i){
            indexTypes.add(concreteArrayType.getIndexType(i));
        }
        valueType = concreteArrayType.getValueType();
        this.parameterizedType = parameterizedType;
    }


    /**
     * store an instantiation of the parameterizedType.
     * @param indexTypes
     * @param valueType
     * @param parameterizedType
     */
    public ArrayInstantiationType(List<BoogieType> indexTypes, BoogieType valueType, ArrayType parameterizedType){
        this.indexTypes = indexTypes;
        this.valueType = valueType;
        this.parameterizedType = parameterizedType;
    }


    public static ArrayType getConcreteType(ArrayAccessExpression e){
        return (new ArrayInstantiationType(e)).getConcreteType();
    }

    /**
     * given an array access expression, get the concrete and the parameterized types from it to stor in an ArrayInstantiationType
     *
     * @param accessExpression
     */
    public ArrayInstantiationType(ArrayAccessExpression accessExpression){
        ArrayType arrayType = (ArrayType)accessExpression.getArray().getType().getUnderlyingType();
        this.valueType = accessExpression.getType();
        this.indexTypes = Arrays.asList(accessExpression.getIndices()).stream().map(expression -> expression.getType()).collect(Collectors.toList());
        this.parameterizedType = arrayType;
    }

    public BoogieType getValueType() {
        return valueType;
    }

    public List<BoogieType> getIndexTypes() {
        return indexTypes;
    }

    public ArrayType getParameterizedType() {
        return parameterizedType;
    }

    /**
     * the type which is produced by replacing type parameters in parameterizedType by their instantiations.
     * @return
     */
    /*
    public ArrayType getInstantiatedType(){
        //TODO assumption: we don't support instantiations by variables which themeselves have type variables.
        //TODO e.g.:    var x:<T>[T]T; var y:<T>[int]T var z:myType
        //TODO          x[y[z]]
        //TODO or do we? should work the same.


        //TODO do we need the scope of the placeholder?
        //TODO if we have a placeholderType, it could be from a type variable from e.g. the procedure declaration,
        //TODO or from <T>[int,T]T the array itself.
        return null;
    }
*/





    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof ArrayInstantiationType)){
            return false;
        }
        ArrayInstantiationType other = (ArrayInstantiationType)obj;
        if(this.getIndexTypes().size() != other.getIndexTypes().size()){
            return false;
        }
        List<Boolean> conditions = new LinkedList<>();
        conditions.add(new Boolean(this.getValueType() == other.getValueType()));
        if(this.getConcreteType() == null){
            conditions.add(other.getConcreteType() == null);
        }else{
            conditions.add(new Boolean(this.getConcreteType().equals(other.getConcreteType())));
        }
        conditions.add(new Boolean(this.getParameterizedType() == other.getParameterizedType()));

        for(int i = 0; i < this.getIndexTypes().size(); ++i){
            conditions.add(new Boolean(this.getIndexTypes().get(i) == other.getIndexTypes().get(i)));
        }
        for(Boolean c: conditions){
            if(!c){
                return false;
            }
        }
        return true;
    }


    public ArrayType getConcreteType(){
        return BoogieType.createArrayType(0,indexTypes.toArray(new BoogieType[indexTypes.size()]),valueType);
        //return new ArrayType(0,indexTypes.toArray(new BoogieType[indexTypes.size()]),valueType);
    }

    @Override
    public int hashCode() {
        return this.indexTypes.toString().hashCode() +
                this.parameterizedType.toString().hashCode() +
                this.valueType.toString().hashCode();
    }


    /**
     * e.g. given
     * [ref, Field int]int
     * <a>[ref, Field a]a
     * we return int, since it is the instantiation of a.
     * @param concreteType must be of same instance as abstract type
     * @param abstractType Can only contain 1 type parameter. Must be of same instance (java class) as concrete type.
     * @return instantiation of type param of abstractType. Placeholder type if concrete type isn't really concrete (i.e. contains type params)
     * null, iff the abstract type contains no type param.
     */
    public static BoogieType getTypeParamInstantiation(BoogieType concreteType, BoogieType abstractType){
        //TODO should we use underlying type??
        if(abstractType instanceof PlaceholderType){
            return concreteType;
        }else if(abstractType instanceof PrimitiveType){
            return null;
        }else if(abstractType instanceof ConstructedType){
            ConstructedType abstractConstructed = (ConstructedType)abstractType.getUnderlyingType();
            ConstructedType concreteConstructed = (ConstructedType)concreteType.getUnderlyingType();

            int paramCount = abstractConstructed.getConstr().getParamCount();
            BoogieType instantiation = null;
            for(int i = 0; i < paramCount; ++i){
                BoogieType abstractParam = abstractConstructed.getParameter(i);
                BoogieType concreteParam = concreteConstructed.getParameter(i);
                instantiation = getTypeParamInstantiation(concreteType,abstractType);
                if(instantiation != null){
                    return instantiation;
                }
            }
            return instantiation;
        }else if(abstractType instanceof ArrayType){
            ArrayType abstractArray = (ArrayType)abstractType;
            ArrayType concreteArray = (ArrayType)concreteType;
            BoogieType instantiation = getTypeParamInstantiation(concreteArray.getValueType(), abstractArray.getValueType());
            if(instantiation != null){
                return instantiation;
            }
            int indexLength = abstractArray.getIndexCount();
            for(int i = 0; i < indexLength; ++i){
                BoogieType concreteIndexType = concreteArray.getIndexType(i);
                BoogieType abstractIndexType = abstractArray.getIndexType(i);
                instantiation = getTypeParamInstantiation(concreteIndexType,abstractIndexType);
                if(instantiation != null){
                    return instantiation;
                }
            }
            return instantiation;
        }else {
            throw new RuntimeException("Error: unknown Boogie type to get type param instantiation.");
        }
    }

    /**
     * given an expression e, return the actual type it has during "runtime". e.g. for ArrayStoreExpression:
     * if the array is IdentifierExpression heap
     * the stored type is <a>[ref, Field a]a;
     * but the actual type can be got from the indices and value, e.g.
     * [ref, Field bool]bool;
     * @param e
     * @return
     */
    public static BoogieType actualType(Expression e){
        BoogieType storedType = e.getType().getUnderlyingType();
        if(!(storedType instanceof ArrayType)){
            return storedType;
        }
        //otherwise we have an array type.
        BoogieType[] indexTypes = null;
        BoogieType valueType = null;
        if(e instanceof ArrayStoreExpression){
            ArrayStoreExpression ase = (ArrayStoreExpression)e;
             indexTypes = Arrays.asList(ase.getIndices()).stream().map(expression -> expression.getType())
                    .collect(Collectors.toList()).toArray(new BoogieType[ase.getIndices().length]);
             valueType= ase.getValue().getType();
        }else if(e instanceof ArrayAccessExpression){
            ArrayAccessExpression aae = (ArrayAccessExpression)e;
            indexTypes = Arrays.asList(aae.getIndices()).stream().map(expression -> expression.getType())
                    .collect(Collectors.toList()).toArray(new BoogieType[aae.getIndices().length]);
            valueType = aae.getArray().getType();
        }else{
            throw new RuntimeException("Error: Couldn't generate type of this array expression");
        }

        return getArrayType(indexTypes,valueType);

    }



    /**
     *
     * @param indexTypes
     * @param valueType
     * @return
     */
    public static ArrayType getArrayType(BoogieType[] indexTypes, BoogieType valueType) {
        HashSet<PlaceholderType> placeholders = new HashSet<PlaceholderType>();
        int numplaceholders = countPlaceHolderTypes(indexTypes, placeholders)
                + countPlaceHolderTypes(valueType, placeholders);
        ArrayType arrtype = BoogieType.createArrayType(numplaceholders,
                indexTypes, valueType);
        return arrtype;
    }

    /**
     * This function counts the number different placeholders in a type this is
     * needed by the BoogieType. Placeholders are somewhat like generics in Java
     *
     * @param types
     * @param alreadyfound
     * @return
     */
    private static int countPlaceHolderTypes(BoogieType[] types,
                                      HashSet<PlaceholderType> alreadyfound) {
        int ret = 0;
        for (int i = 0; i < types.length; i++) {
            ret += countPlaceHolderTypes(types[i], alreadyfound);
        }
        return ret;
    }

    /**
     * This function counts the number different placeholders in a type this is
     * needed by the BoogieType. Placeholders are somewhat like generics in Java
     *
     * @param type
     * @param alreadyfound
     * @return
     */
    private static int countPlaceHolderTypes(BoogieType type,
                                      HashSet<PlaceholderType> alreadyfound) {
        if (type instanceof PrimitiveType) {
            return 0;
        } else if (type instanceof ArrayType) {
            ArrayType arrtype = (ArrayType) type;
            return arrtype.getNumPlaceholders();
        } else if (type instanceof ConstructedType) {
            ConstructedType contype = (ConstructedType) type;
            int ret = 0;
            for (int i = 0; i < contype.getConstr().getParamCount(); i++) {
                ret += countPlaceHolderTypes(contype.getParameter(i),
                        alreadyfound);
            }
            return ret;
        } else if (type instanceof PlaceholderType) {
            if (alreadyfound.contains(type)) {
                return 0;
            }
            alreadyfound.add((PlaceholderType) type);
            return 1;
        }
        return 0;
    }

    /**
     * given an arrayType with type parameters, returns one where there are no parameters, since all are replaced with the dummy type.
     * @param arrayType
     * @return
     */
    public static ArrayType generateDummyInstantiation(ArrayType arrayType){
        BoogieType newValueType = generateDummyInstantiationHelper(arrayType.getValueType());
        BoogieType[] newIndexTypes = new BoogieType[arrayType.getIndexCount()];
        for(int i = 0; i < newIndexTypes.length; ++i){
            newIndexTypes[i] = generateDummyInstantiationHelper(arrayType.getIndexType(i));
        }
        return getArrayType(newIndexTypes,newValueType);
    }

    private static BoogieType generateDummyInstantiationHelper(BoogieType boogieType){
return null;
    }

}
