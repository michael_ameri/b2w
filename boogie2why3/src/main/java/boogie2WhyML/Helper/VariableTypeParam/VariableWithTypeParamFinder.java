package boogie2WhyML.Helper.VariableTypeParam;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import boogie.type.*;
import util.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * TODO: This class can be used in the future to translate map type with type variable of the form x:<a>[int]a
 * finds all variable declarations v:t, where t contains a map with a type param, when it is expanded.
 * e.g.
 * t = <a>[int,a]a
 * v: t
 */
public class VariableWithTypeParamFinder implements ASTVisitor {

    //todo: also need to find local variables.

    /**
     * requires the boogie types to have been filled by a typechecker.
     * @param programFactory
     */
    public VariableWithTypeParamFinder(ProgramFactory programFactory){
        findVariablesWithParamFinder(programFactory);
    }

    /**
     * this doesn't have to be called after constructor. But can be called if a new PF should be processed.
     * @param programFactory
     * @return
     */
    public List<String> findVariablesWithParamFinder(ProgramFactory programFactory){
        this.resultVariableNames = new LinkedList<>();
        for(Declaration d: programFactory.getASTRoot().getDeclarations()){
            d.accept(this);
        }
        return resultVariableNames;
    }



    /**
     * This can be called after constructor, and after each call to findVariablesWithParamFinder with a new PF.
     * @return name of variables which meet the criteria to be found by this class.
     */
    public List<String> getResultVariableNames() {
        return resultVariableNames;
    }

    /**
     * this has no relation to the PF. it just goes through all local variables of a proceudre or implementation, and returns those
     * which have type parameters.
     * @param declaration
     * @return
     */
    public static List<String> localVariableNames(ProcedureOrImplementationDeclaration declaration){
        //TODO untested
        List<String> result = new LinkedList<>();
        if(declaration.getBody() == null || declaration.getBody().getLocalVars() == null){
            return result;
        }
        for(VariableDeclaration variableDeclaration: declaration.getBody().getLocalVars()){
            for (VarList varList: variableDeclaration.getVariables()){
                if(containsTypeParams(varList.getType().getBoogieType().getUnderlyingType())){
                    result.addAll(Arrays.asList(varList.getIdentifiers()));
                }
            }
        }
        return result;
    }

    private List<String> resultVariableNames = new LinkedList<>();

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }



    @Override
    public void visit(VarList varList) {
        BoogieType actualType = varList.getType().getBoogieType().getUnderlyingType();
        if(containsTypeParams(actualType)){
            resultVariableNames.addAll(Arrays.asList(varList.getIdentifiers()));
        }
    }

    /**
     *containsTypeParams is set to true, iff these variables type contains at least one map with at least 1 parameter.
     * @param boogieType
     * @return
     */
    public static boolean containsTypeParams(BoogieType boogieType){
        BoogieType actualType = boogieType.getUnderlyingType();
        if(actualType instanceof ArrayType){
            //return true if this arrayType has params. otherwise check if one of index types of value type is a map type with type params.
            ArrayType arrayType = (ArrayType)actualType;
            if(arrayType.getNumPlaceholders() > 0){
                return true;
            }else{
                for(int i = 0; i < arrayType.getIndexCount(); ++i){
                    if(containsTypeParams(arrayType.getIndexType(i))){
                        return true;
                    }
                }
                return containsTypeParams(arrayType.getValueType());
            }
        }else if(actualType instanceof ConstructedType){
            ConstructedType constructedType = (ConstructedType)actualType;
            for(int i = 0; i < constructedType.getConstr().getParamCount(); ++i){
                if(containsTypeParams(constructedType.getParameter(i))){
                    return true;
                }
            }
            return false;
        }else if(actualType instanceof PlaceholderType){
            //todo might need to rename this method or split into 2 methods depending on what is wanted.
            //depends: type set a = [a]int contains placeholder, but no type parameter. What is searched for?
            Log.error("Is placeholdertype a type param? ");
            return false;
        }else if(actualType instanceof PrimitiveType){
            return false;
        }else{
            throw new RuntimeException("Error: unknown Boogie type");
        }
    }


    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        for(VarList varList: variableDeclaration.getVariables()){
            varList.accept(this);
        }
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
