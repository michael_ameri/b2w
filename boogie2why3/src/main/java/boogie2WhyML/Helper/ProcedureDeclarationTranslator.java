package boogie2WhyML.Helper;

import boogie.ast.Body;
import boogie.ast.statement.Label;
import boogie.ast.statement.Statement;
import boogie2WhyML.BoogieTranslator;
import util.Log;
import whyML.ast.Param;
import whyML.ast.declerations.Declaration;
import whyML.ast.declerations.Lemma;
import whyML.ast.expression.*;
import whyML.ast.formula.BinaryFormula;
import whyML.ast.formula.Formula;
import whyML.ast.formula.TrueFormula;
import whyML.ast.formula.quantifier.FormulaBinder;
import whyML.ast.formula.quantifier.QuantifiedFormula;
import whyML.ast.spec.Postcondition;
import whyML.ast.spec.Precondition;
import whyML.ast.spec.Spec;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class to translate procedureDeclarations.
 */
public class ProcedureDeclarationTranslator {


    public static Expression generateLetDeclarationExpression(Body body, Expression preambleExpression, List<Param> outputArguments, BoogieTranslator boogieTranslator){
        //generate body expression, surround it with try catch for return statemetn, add preamble to it.
        List<Expression> bodyExpressionList = new LinkedList<>();
        for(Statement s: body.getBlock()){
            boogieTranslator.setFormulaTermExpression(BoogieTranslator.FormulaTermExpression.EXPRESSION);
            if(s instanceof Label){
                Log.error("Skipped adding label to declaration list.");
                continue;
            }
            s.accept(boogieTranslator);
            bodyExpressionList.add((whyML.ast.expression.Expression)boogieTranslator.getResultNode());
        }
        //always have an assume true at the end.. in case e.g. body is empty.
        bodyExpressionList.add(new AssertionExpression(AssertionExpression.AssertionType.ASSUME, new TrueFormula()));
        whyML.ast.expression.Expression bodyExpression = convertExpressionListToSingleExpression(bodyExpressionList);
        whyML.ast.expression.Expression returnExpression;
        if(outputArguments.size() == 1){
            returnExpression = new SymbolExpression(boogieTranslator.whyMLIdentifier(outputArguments.get(0).getName().trim())   +".contents");
        }else if(outputArguments.size() > 1){
            List<whyML.ast.expression.Expression> tupleExpressions = new LinkedList<>();
            for(Param p: outputArguments){
                tupleExpressions.add(new SymbolExpression(boogieTranslator.whyMLIdentifier(p.getName().trim())+".contents"));
            }
            returnExpression = new TupleExpression(tupleExpressions);
        }else{
            //do nothing?
            returnExpression = new AssertionExpression(AssertionExpression.AssertionType.ASSUME,new TrueFormula());
        }
        whyML.ast.expression.Expression bodyExpWithReturn = surroundExpressionWithExceptionCatching(bodyExpression,new Handler("Return ", new AssertionExpression(AssertionExpression.AssertionType.ASSUME,new TrueFormula())));
        bodyExpWithReturn = new CodeMarkExpression("Start", bodyExpWithReturn);
        List<whyML.ast.expression.Expression> letDeclerationExpressionList = new LinkedList<whyML.ast.expression.Expression>();
        letDeclerationExpressionList.add(preambleExpression);
        letDeclerationExpressionList.add(bodyExpWithReturn);
        letDeclerationExpressionList.add(returnExpression);
        return convertExpressionListToSingleExpression(letDeclerationExpressionList);
    }

    /**
     *
     * @param expression
     * @param handler
     * @return the input expression surrounded with a "try .. with" exception handling. Null if either of the inputs are null.
     */
    public static ExceptionCatchingExpression surroundExpressionWithExceptionCatching(whyML.ast.expression.Expression expression, Handler handler){
        if(expression == null || handler == null){
            return null;
        }
        return new ExceptionCatchingExpression(expression,handler);
    }

    /**
     *
     * @param expressionList
     * @return null if list is null or empty. first element if list has length 1. A SequenceExpression if list has >= 2 elements.
     */
    public static whyML.ast.expression.Expression convertExpressionListToSingleExpression(List<whyML.ast.expression.Expression> expressionList){
        if(expressionList == null || expressionList.isEmpty()){
            return null;
        }
        if(expressionList.size() == 1){
            return expressionList.get(0);
        }

        SequenceExpression sequenceExpression= new SequenceExpression(expressionList.get(0),null);
        for(int i = 1; i < expressionList.size() - 1; ++i){
            sequenceExpression.setExp2(expressionList.get(i));
            sequenceExpression = new SequenceExpression(sequenceExpression,null);
        }
        sequenceExpression.setExp2(expressionList.get(expressionList.size()-1));
        return sequenceExpression;
    }




    /**
     * generates a lemma expression, of the form forall binders . (pre1 && pre2 && ... && pren) -> (post1 && .. && postn)
     * if binders is null or empty, the forall part is left away.
     * @param preconditions
     * @param postconditions
     * @return
     */
    public static Declaration generateLemma(String lemmaIdent, List<FormulaBinder> binders, List<Precondition> preconditions, List<Postcondition> postconditions){
        List<Formula> preconditionFormulas = preconditions.stream().map(precondition -> precondition.getFormula()).collect(Collectors.toList());
        List<Formula> postconditionFormulas = postconditions.stream().map(postcondition -> postcondition.getFormula()).collect(Collectors.toList());
        BinaryFormula implication = new BinaryFormula(ExpressionJoiner.conjunction(preconditionFormulas, true), "->", ExpressionJoiner.conjunction(postconditionFormulas,true));
        QuantifiedFormula qf = new QuantifiedFormula(true,binders,implication);
        return new Lemma(lemmaIdent, qf);
    }

}
