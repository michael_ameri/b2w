type a;
type b = int;
type c = [a]bool;


var x0: int where x0 > 3;
var x1,x2,x3: a;
var x4,x5: c;
var x6, x7, x8: int where x7 > 5;


var v1,v2: int, v3,v4: bool;


procedure p() returns (res:int)
modifies x7;
modifies x8;
requires 1 > 0;
ensures 2 < 3;
{
  assert x0 > 3;
  x7 := 1;
  assert x7 == 1;
  havoc x8;
  assert false; //verifiable because havoc x8 says assume (x7 > 5); (because of whereClause)
}