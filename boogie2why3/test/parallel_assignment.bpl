procedure p(){
  var i,j: int;
  var h1,h2,h3: [int]bool;
  j:=2;
  h1[2] := false;
  i,j,h1,h2,h3[1] := j+1,1,h1[2 := true],h1,false;
  assert j == 1;
  assert i == 3;//not 2
  assert h1[2];
  assert !h2[2];
  assert !h2[1];
}
            