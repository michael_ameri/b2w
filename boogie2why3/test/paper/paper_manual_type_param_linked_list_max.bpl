type ref;
type Field _;
type Heap = <alpha>[ref, Field alpha]alpha;

const unique C.data: Field int;
const unique C.next: Field ref;
const unique alloc: Field bool;


const null: ref;

var heap: Heap;

function max(x,y:int): int
{
    if(x >= y)then
        x
    else
        y
}


procedure max_value(root: ref) returns(result:ref)
ensures (root == null) ==> (result == null);
ensures ((root != null) && (heap[root,C.next] == null)) ==> (result == root);//next element null => current element is largest in list
ensures ((root != null) && (heap[root,C.next] != null)) ==> (heap[result, C.data] >= max(heap[root, C.data], heap[heap[root,C.next],C.data]));
{
    var root_value: int;
    var next_node: ref;
    var next_value: int;

    var rest_max: ref;
    var rest_max_value:int;

    root_value := heap[root, C.data];
    if(root == null){
        result := root;
    }else{
        next_node := heap[root,C.next];
        if(next_node == null){
            result := root;
        }else{
            next_value := heap[next_node, C.data];
            call rest_max := max_value(next_node);
            rest_max_value := heap[rest_max, C.data];
            if(root_value >= rest_max_value){
              result := root;
            }else{
              result := rest_max;
            }
        }
    }
}