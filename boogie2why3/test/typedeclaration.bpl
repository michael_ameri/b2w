type aa;
type g;
type bb = int;
type cc = [int]bool;
type x = cc;
type y y1 y2;
type v v1 v2 = y v1 v2;
type w v1 v2 = y aa v2;
type z z1 z2 = [z1,z1,z1] z2;
type dd z4 z3 = y int z4;

type e e1 e2;
type f f1 f2 = e (y f1 f2) f2;