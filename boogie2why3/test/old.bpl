var v:int where v == 5;
procedure F(n: int) returns (r: int)
modifies v;
ensures r == old(v) -3;
ensures v == old(v) - 3;
{
  assert v == old (v);
  v := 2;
  assert v < old (v);
  r := v;
}
