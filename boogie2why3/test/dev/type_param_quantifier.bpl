type ref;
type Field _;
type Heap = <a>[ref, Field a]a;
type Frame = <a>[ref, Field a]bool;


procedure F(n: int) returns (r: int)
{
    var heap: Heap;
    var h0, h1: Heap;
    var o: ref;
    //a is instantiatiated to bool and int
    assume (forall <a> x: Field a:: (heap[o,x] == true) && (heap[o,x] == 3));
    //a is not instantiated. needs to be all concrete types.
    assume (forall <a> x: Field a:: h0[o,x] == h1[o,x]);
    //combination of the 2 above.
    assume (forall <a> x: Field a:: h0[o,x] == h1[o,x] && (heap[o,x] == true) && (heap[o,x] == 3));
}
