type Rel u v = [u,v]bool;

function Rel#Empty<U, V>(): Rel U V;

function rel_bool<U, V>(r: Rel U V): bool;

axiom (forall <u,v> r: Rel u v :: rel_bool(r) );

axiom (forall <u,v> r: Rel u v :: Rel#Empty() == r);

type Set T = [T]bool;

function Rel#Domain<U, V>(Rel U V): Set U;

function {: inline } Set#IsEmpty<T>(s: Set T): bool;

//translation ok up to here.

//type parameters U and V are ambiguous, both instantiated to int.
//TODO how to detect?
axiom (forall<U, V> r: Rel U V :: { Rel#Domain(r), Rel#Empty() }{ Set#IsEmpty(Rel#Domain(r)) }
  (Set#IsEmpty(Rel#Domain(r)) <==> r == Rel#Empty()));