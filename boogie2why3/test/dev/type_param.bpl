//3 parts:

type Field _;
type ref;
type HeapType = <alpha> [ref, Field alpha]alpha;

const Void: ref;
const closed: Field bool;
const owner: Field ref;

//PART 1:
//alpha in h is instantiated with two different types: bool and ref.
function in_domain(h: HeapType, o: ref, o': ref): bool
{
	o == o' || ( h[o, closed] && h[o', closed] && in_domain(h, o, h[o', owner]) )
}

//PART 2:
//(from dafny) these should NOT be changed.
function Map#Domain<U,V>(Map U V) : [U]bool;
type Map _ _;
type Ty;

function Map#Elements<U,V>(Map U V) : [U]V;

function Map#Card<U,V>(Map U V) : int;

axiom (forall<U,V> m: Map U V :: { Map#Card(m) } 0 <= Map#Card(m));

function $Is<T>(a:T, b:Ty) : bool;

function Map#Empty<U,V>() : Map U V;

axiom (forall<U,V> m: Map U V ::
  { Map#Card(m) }
  (Map#Card(m) == 0 <==> m == Map#Empty())
     && (Map#Card(m) != 0 ==> (exists x: U :: Map#Domain(m)[x])));


//PART 3:
function tricky <T>(a:HeapType, b: Field bool, c: Field T, d: Field T, o: ref) : T
{
    if(a[o,b]) then
        a[o,c]
    else
        a[o,d]
}



//PART 4:
var x:<T>[T]T; var y:<T>[int]T; var z:int;
procedure p(input:int) returns (res:int)
{
  res := x[y[input]];
}


//PART 5:
const allocated: Field bool; // Ghost field for allocation status of objects
// Set of object-field pairs
type Frame = <alpha>[ref, Field alpha]bool;

// Frame is closed under ownership domains and includes all unallocated objects
function { :inline } closed_under_domains(frame: Frame, h: HeapType): bool
{
  (forall <U> o': ref, f': Field U :: {frame[o', f']}
    !h[o', allocated] || (exists <V> o: ref, f: Field V :: frame[o, f] && in_domain(h, o, o') && o != o') ==> frame[o', f'])
}

//PART 6:

procedure p6() returns(res:bool)
{
  var f1,f2: Frame;
  var h: HeapType;
  res := closed_under_domains(f1,h) == closed_under_domains(f2,h);
}

//PART 7:
//simple example
type aMap = <a>[a]a;
var v: aMap;
procedure p(x: aMap) returns (r: int)
{
    r := x[2];
    assume x[true];
    assume x[2] == 2;
    assert f(x);

    assume v[true];
    assume v[2] == 2;
    assert f(v);
}
function f(x:aMap) returns (bool);
axiom (forall x:aMap :: f(x) == (x[true] && (x[2]==2)));


type aMap 'a = map 'a 'a
let p (x: aMap 'a): (int)
=
    let r = get (x:aMap int) 2 in
    assume {get (x:aMap bool) true};
    r

type aMap 'a = map 'a 'a
let p (x: aMap 'a): (int)
=
    let x_int: aMap int = x in
    let x_bool: aMap bool = x in
    let r = get x_int 2 in
    assume {get x_bool true};
    r

function f(x: aMap 'a) : bool;
axiom F0: forall x: aMap 'a. (f x) = (get x true)