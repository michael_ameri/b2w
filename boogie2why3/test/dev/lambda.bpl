procedure l1(){
  //we rewrite an inline lambda expression as an axiomatized function to get the same effect.
  var a,b: [int,real]int;
  a := (lambda x:int, y:real :: x + 1);
  b := lambda1();
  assert (forall x:int, y:real:: b[x,y] == a[x,y]);
}
//declaration and axiomatization of lambda expression from procedure above.
function lambda1() returns (res:[int,real]int);
axiom (forall x:int, y:real :: lambda1()[x,y] == x+1);

////////////////////////////////////////////////////////

function l2(x: int, y: int):[real]bool
{(lambda z: real:: true)}
function lambda2() returns (res:[real]bool);
axiom (forall x:real:: lambda2()[x] == true);

//this function has the same effect as l2.
function l2_equivalent(x: int, y:int):[real]bool
{lambda2()}
