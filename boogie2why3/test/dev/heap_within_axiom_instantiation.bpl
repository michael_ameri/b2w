type ref;
type Field _;
type Heap = <alpha>[ref,Field alpha]alpha;

const unique alloc: Field bool;

function $HeapSucc(Heap, Heap) : bool;

//translation of axiomatization of this works fine
function {:inline true} read<alpha>(H: Heap, r: ref, f: Field alpha) : alpha
{
  H[r, f]
}

//translation of this is not allowed -> function read needs to explicitly return bool in WhyML (need function read_bool)
//-> h needs to be of type [ref, Field bool]bool;
axiom (forall h: Heap, k: Heap ::
  { $HeapSucc(h, k) }
  $HeapSucc(h, k)
     ==> (forall o: ref :: { read(k, o, alloc) } read(h, o, alloc) ==> read(k, o, alloc)));

const unique aRef: ref;
var heap: Heap where heap[aRef, alloc];
var fr: Field ref;
procedure p(x:Heap, o: ref, rand: Field int)
modifies heap;
{
  if(*){
    assert x[o, alloc];
  }
  assert x[o, alloc];
  assert x[o, rand] == 4;
  assert heap[o, alloc];
  assert heap[o, rand] == 4;
  heap[o,rand] := 3;
  heap[o,fr] := o;
  //assert (forall <a> f: Field a, v: a :: heap[o,f] == v );
}

