
type ref;
type Field _;

type Frame = <alpha>[ref, Field alpha]bool;
type HeapType = <alpha>[ref, Field alpha]alpha; // Type of a heap (with generic field subtype and generic content type);

var x1: HeapType;
var x2: HeapType;


procedure p0()
modifies x1, x2;
ensures x1 == x2;
{
    var l: HeapType;
    x2 := x1;
    assume (forall r: ref, f: Field bool:: x1[r,f]);
}


////////////////////


var heap1 : <a>[a]int;
var heap2 : <a>[a]int;

procedure p();
ensures heap1[2] == heap2[true];

procedure p2()
ensures heap1[true] == heap1[1];
{
  //assume heap1[true] == heap1[1];
  assume (forall <a,b>x:a,y:b :: heap1[x] == heap1[y]);
}

procedure p3()
ensures heap1[true] == heap1[1];
modifies heap1;
{
  //assume heap1[true] == heap1[1];
  heap1[true] := heap1[1];
}


type i = <a>[a]int;

type t = [[<a>[a]<b>[b]i]bool]int;

var v: [[<a>[a]<b>[b]int]bool]int;
var v1: t;



type t2 = t;
var v2: t2;


var v3: Field <a>[a]bool;
var v4: bool;
var v5: [int]bool;
