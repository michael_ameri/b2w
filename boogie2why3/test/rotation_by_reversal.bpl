// Copyright 2014 Carlo A. Furia
// Sequence (0-indexed) of a[low..high)
function seq(a: [int]int, low: int, high: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i: int ::
      0 <= i && i < high - low
      ==>
      seq(a, low, high)[i] == a[low + i]);


// i mod N
function wrap(i: int, N: int) returns(int);
axiom (forall i, N: int :: 0 <= i && i < N ==> wrap(i, N) == i);
axiom (forall i, N: int :: 0 < N && N <= i ==> wrap(i, N) == wrap(i - N, N));

procedure lemma_wrap_bounds(i: int, N: int)
   requires 0 < N;
   requires 0 <= i;
   ensures 0 <= wrap(i, N) && wrap(i, N) < N;
{
   // proof by induction
   if (i < N) {
   } else {
     call lemma_wrap_bounds(i - N, N);
   }
}


// Left-rotated sequence of a[low..high) at r
function rot(a: [int]int, low: int, high: int, r: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i, r: int ::
      0 <= r && r < high - low && 0 <= i && i < r
      ==>
      rot(a, low, high, r)[i + high - low - r] == seq(a, low, high)[i]);
axiom (forall a: [int]int, low: int, high: int, i, r: int ::
      0 <= r && r < high - low && r <= i && i < high - low
      ==>
      rot(a, low, high, r)[i - r] == seq(a, low, high)[i]);


// Equivalent representation of indexes
procedure lemma_rot(a: [int]int, low, high: int, r: int, p: int)
   requires low <= high;
   requires 0 <= r && r < high - low;
   requires 0 <= p && p < high - low;
   ensures rot(a, low, high, r)[p] == seq(a, low, high)[wrap(r + p, high - low)];
{
}


// Reverse sequence of a[low..high)
function rev(a: [int]int, low: int, high: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i: int ::
      0 <= i && i < high - low
      ==>
      rev(a, low, high)[i] == seq(a, low, high)[high - low - 1 - i]);

// Reversal is its own inverse
procedure lemma_rev_rev(a: [int]int, low: int, high: int, p: int)
   requires low <= high;
   requires 0 <= p && p < high - low;
   ensures rev(rev(a, low, high), 0, high - low)[p] == seq(a, low, high)[p];
{
}

// Reversal of concatenation: rev(a[al..ah) b[bl..bh)) == rev(b[bl..bh)) rev(a[al..ah))
procedure lemma_rev_cat(a: [int]int, al: int, ah: int,
                        b: [int]int, bl: int, bh: int,
                        c: [int]int, p: int)
   requires al <= ah;
   requires bl <= bh;
   requires 0 <= p && p < bh - bl + ah - al;
   requires (forall i: int :: 0 <= i && i < ah - al ==> c[i] == seq(a, al, ah)[i]);
   requires (forall i: int :: 0 <= i && i < bh - bl ==> c[ah - al + i] == seq(b, bl, bh)[i]);
   ensures 0 <= p && p < bh - bl
           ==>
           rev(c, 0, ah - al + bh - bl)[p] == rev(b, bl, bh)[p];
   ensures bh - bl <= p && p < bh - bl + ah - al
           ==>
           rev(c, 0, ah - al + bh - bl)[p] == rev(a, al, ah)[p - (bh - bl)];
{
}

// Other derived representations of reversal of concatenation
procedure lemma_rev_cat_2(t: [int]int, tl: int, th: int,
                          s: [int]int, sl: int, sh: int,
                          c: [int]int, p: int)
   requires tl <= th;
   requires sl <= sh;
   requires 0 <= p && p < sh - sl + th - tl;
   requires (forall i: int ::
            0 <= i && i < th - tl
            ==>
            seq(c, 0, th - tl)[i] == rev(t, tl, th)[i]);
   requires (forall i: int :: 0 <= i && i < sh - sl
            ==>
            seq(c, th - tl, sh - sl + th - tl)[i] == rev(s, sl, sh)[i]);
   ensures 0 <= p && p < sh - sl
           ==>
           rev(c, th - tl, th - tl + sh - sl)[p] == seq(s, sl, sh)[p];
   ensures 0 <= p && p < th - tl
           ==>
           rev(c, 0, th - tl)[p] == seq(t, tl, th)[p];
   ensures 0 <= p && p < sh - sl
           ==>
           rev(c, 0, th - tl + sh - sl)[p] == seq(s, sl, sh)[p];
   ensures 0 <= p && p < th - tl
           ==>
           rev(c, 0, th - tl + sh - sl)[p + sh - sl] == seq(t, tl, th)[p];
{
}

// The position i maps to in a reversal of [low..high)
function rp(i: int, low: int, high: int) returns(int);
axiom (forall i, low, high: int ::
      rp(i, low, high) == high - (i - low) - 1);


// This is the only dependency on rotation_theory.bpl in reversal.bpl
// Relation between rot and wrap
procedure lemma_rotwrap(a: [int]int, low, high: int, r: int, p: int)
   requires low <= high;
   requires 0 <= r && r < high - low;
   requires 0 <= p && p < high - low;
   ensures seq(a, low, high)[p] == rot(a, low, high, r)[wrap(p + high - low - r, high - low)];
{
}


procedure reverse(a: [int]int, low, high: int) returns(b: [int]int)
   ensures (forall i: int ::
           0 <= i && i < high - low
           ==>
           seq(b, low, high)[i] == rev(a, low, high)[i]);
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
{
   var s, d: int;
   b := a;
   if (low >= high) {
      return;
   }
   s := low;
   d := high - 1;
   assert d == rp(s, low, high);
   while (s < high)
   invariant (low <= s && s <= high);
   invariant (d == rp(s, low, high));
   // invariant (s < high ==> low <= d && d < high);
   invariant (forall i: int :: low <= i && i < s ==> a[i] == b[rp(i, low, high)]);
   invariant (forall i: int :: i < low ==> b[i] == a[i]);
   invariant (forall i: int :: high <= i ==> b[i] == a[i]);
   {
      b[d] := a[s];
      s := s + 1;
      d := d - 1;
   }
}


procedure reverse_inplace(a: [int]int, low, high: int) returns(b: [int]int)
   ensures (forall i: int ::
           0 <= i && i < high - low
           ==>
           seq(b, low, high)[i] == rev(a, low, high)[i]);
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
{  // If variables s, d are renamed p, q, verification takes ~9x more time!
   var s, d, t: int;
   b := a;
   if (low >= high) {
      return;
   }
   s := low;
   d := high - 1;
   assert d == rp(s, low, high);
   while (s < d + 1)  // swapping with itself the central element of odd-sized arrays
   invariant (low <= s && s <= d + 2 && d + 1 <= high);
   invariant (d == rp(s, low, high));
   // invariant (s < d + 1 ==> s <= d && d < high);
   invariant (forall i: int :: low <= i && i < s ==> a[i] == b[rp(i, low, high)]);
   invariant (forall i: int :: d < i && i < high ==> a[i] == b[rp(i, low, high)]);
   invariant (forall i: int :: s <= i && i <= d ==> b[i] == a[i]);
   invariant (forall i: int :: i < low ==> b[i] == a[i]);
   invariant (forall i: int :: high <= i ==> b[i] == a[i]);
   {
      t := b[d];
      b[d] := b[s];
      b[s] := t;
      s := s + 1;
      d := d - 1;
   }
}


// Reversing a sequence twice leaves it unchanged
procedure rev_twice(a: [int]int, low, high: int) returns(b: [int]int)
   ensures (forall i: int ::
           0 <= i && i < high - low
           ==>
           seq(b, low, high)[i] == seq(a, low, high)[i]);
{
   b := a;
   call b := reverse(b, low, high);
   call b := reverse(b, low, high);
}


// Left-rotate a by r by performing three reversals.
// Key correctness argument: if |X| = r and |Y| = N - r,
//                           then rot(X Y, r) = rev(rev(X) rev(Y)) = Y X
procedure rotate_reverse(a: [int]int, N: int, r: int) returns(b: [int]int)
  requires 0 <= r && r < N;
  ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   b := a;
   call b := reverse(b, 0, r);
   call b := reverse(b, r, N);
   assert (forall i: int :: 0 <= i && i < r ==> seq(b, 0, r)[i] == rev(a, 0, r)[i]);
   assert (forall i: int :: 0 <= i && i < N - r ==> seq(b, r, N)[i] == rev(a, r, N)[i]);
   //call forall lemma_rev_cat_2(a, 0, r, a, r, N, b, *);
   assume (forall p:int:: ((0 <= r)&&(r<=N) && (0<=p) && (p<N-r+r-0) && (forall i:int::0 <= i && i < r - 0
                                                                                         ==>
                                                                                         seq(b, 0, r - 0)[i] == rev(a, 0, r)[i])
                           && ((forall i: int :: 0 <= i && i < N - r
                                           ==>
                                           seq(b, r - 0, N - r + r - 0)[i] == rev(a, r, N)[i])))

                           ==>((0 <= p && p < N - r
                                          ==>
                                          rev(b, r - 0, r - 0 + N - r)[p] == seq(a, r, N)[p])
                              &&
                              (0 <= p && p < r - 0
                                         ==>
                                         rev(b, 0, r - 0)[p] == seq(a, 0, r)[p])
                              &&
                              (0 <= p && p < N - r
                                          ==>
                                          rev(b, 0, r - 0 + N - r)[p] == seq(a, r, N)[p])
                              &&
                              (0 <= p && p < r - 0
                                          ==>
                                          rev(b, 0, r - 0 + N - r)[p + N - r] == seq(a, 0, r)[p])
                              )
         );



   assert (forall i: int :: 0 <= i && i < r ==> rev(b, 0, r)[i] == seq(a, 0, r)[i]);
   assert (forall i: int :: 0 <= i && i < N - r ==> rev(b, r, N)[i] == seq(a, r, N)[i]);
   call b := reverse(b, 0, N);
   assert (forall i: int :: 0 <= i && i < N - r ==> seq(b, 0, N)[i] == seq(a, r, N)[i]);
   assert (forall i: int :: 0 <= i && i < r ==> seq(b, 0, N)[i + N - r] == seq(a, 0, r)[i]);
   //call forall lemma_rot(a, 0, N, r, *);
   assume (
        forall p:int:: (
            (0 <= N) && (0<= r && r < N - 0) && (0 <= p && p < N - 0)
            ==>
            rot(a, 0, N, r)[p] == seq(a, 0, N)[wrap(r + p, N - 0)]
        )
   );
   assert (forall i: int :: 0 <= i && i < N - r ==> rot(a, 0, N, r)[i] == seq(b, 0, N)[i]);
}

