procedure p1() returns (i:int,b:bool);
ensures i == 0;
ensures b;


procedure p()
{
    var x:int;
    var y: bool;
    call x,y := p1();
    assert x == 0;
}